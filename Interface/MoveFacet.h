
#pragma once

#include "GLAppGui/GLWindow.h"
#include "Vector.h"
#include <memory>

#if defined(SYNRAD)
#include "SynradTypes.h"
#endif

class GLButton;
class GLTextField;
class GLLabel;
class GLRadioButton;
class GLRadioGroup;
class GLToggle;
class InterfaceGeometry;
class Worker;
class GLTitledPanel;

class MoveFacet : public GLWindow {

public:

  // Construction
  MoveFacet(InterfaceGeometry *interfGeom,Worker *work);

  // Implementation
  void ProcessMessage(GLComponent *src,int message) override;

private:
  
  InterfaceGeometry     *interfGeom;
  Worker	   *work;

  std::shared_ptr<GLRadioGroup> offsetDirectionRadioGroup;
  GLRadioButton* offsetRadioButton;
  GLRadioButton* directionRadioButton;
  GLLabel* directionLabel;
  GLLabel* distanceLabel;
  GLTextField	*distanceText;
  GLLabel* cmLabelDistance;
  GLLabel	*dxLabel;
  GLTextField	*xText;
  GLLabel	*cmLabelX;
  GLLabel	*cmLabelY;
  GLTextField	*yText;
  GLLabel	*dyLabel;
  GLLabel	*cmLabelZ;
  GLTextField	*zText;
  GLLabel	*dzLabel;
  GLTitledPanel	*twoPointsPanel;
  GLButton	*dirFacetCenterButton;
  GLButton	*dirVertexButton;
  GLButton	*baseFacetCenterButton;
  GLButton	*baseVertexButton;
  GLButton	*facetNormalButton;
  GLLabel	*directionStatusLabel;
  GLLabel	*baseStatusLabel;
  GLTitledPanel* facetPanel;
  GLLabel *facetSelectedLabel;
  GLLabel *facetSelectedValueLabel;
  GLButton *facetUButton;
  GLButton *facetVButton;
  GLButton *facetNXButton;
  GLButton *facetNYButton;
  GLButton *facetNZButton;

#if defined (SYNRAD)
  GLTitledPanel* trajPanel;
  GLTitledPanel* dirTrajPanel;
  GLTitledPanel* baseTrajPanel;
  GLButton* dirTrajPointButton;
  GLButton* baseTrajPointButton;
  GLToggle* trajRotationCheckbox;
  GLLabel* trajDirectionStatusLabel;
  GLLabel* trajBaseStatusLabel;
  Trajectory_Point baseTrajPoint;
  Vector3d rotAxisP0;
  Vector3d rotAxisDir;
  double rotAngle = 0;
  GLLabel* rotAxisLabel;
  GLLabel* rotAngleLabel;
#endif

  GLButton	*copyButton;
  GLButton	*moveButton;
  GLTitledPanel	*dirPanel;
  GLTitledPanel	*basePanel;

  Vector3d baseLocation;
  bool baseLocationSet = false;

  void SwitchToDirectionMode();
  void SwitchToOffsetMode();

#if defined(SYNRAD)
  std::vector<size_t> GetSelectedRegions();
#endif
};