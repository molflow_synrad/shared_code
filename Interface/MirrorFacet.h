

/*
  File:        MirrorFacet.h
  Description: Mirror facet to plane dialog
*/
#ifndef _MIRRORFACETH_
#define _MIRRORFACETH_

#include "GLAppGui/GLWindow.h"
#include "Geometry_shared.h" //UndoPoint
#include <vector>
#include <memory>

class InterfaceGeometry;
class Worker;
class GLButton;
class GLTextField;
class GLLabel;
class GLRadioGroup;
class GLRadioButton;
class GLTitledPanel;

class MirrorFacet : public GLWindow {

public:
  // Construction
  MirrorFacet(InterfaceGeometry *interfGeom,Worker *work);
  void ClearUndoVertices();
  void ProcessMessage(GLComponent *src,int message) override;

  // Implementation
private:

  void OnRadioUpdate(GLComponent *src);
  
  GLTitledPanel *iPanel;
  GLButton     *mirrorButton,*mirrorCopyButton;
  GLButton    *projectButton,*projectCopyButton,*undoProjectButton;
  GLButton	  *getSelFacetButton;
  std::shared_ptr<GLRadioGroup> planeDefinitionRadioGroup;
  GLRadioButton* xyPlaneRadioButton;
  GLRadioButton* yzPlaneRadioButton;
  GLRadioButton* xzPlaneRadioButton;
  GLRadioButton* facetPlaneRadioButton;
  GLRadioButton* selectedVerticesRadioButton;
  GLRadioButton* planeEquationRadioButton;
  GLTextField *aText;
  GLTextField *bText;
  GLTextField *cText;
  GLTextField *dText;
  GLTextField *facetNumber;
  GLLabel		*aLabel;
  GLLabel		*bLabel;
  GLLabel		*cLabel;
  GLLabel		*dLabel;

  int nbFacetS;
  int    planeMode;
  std::vector<UndoPoint> undoPoints;

  InterfaceGeometry     *interfGeom;
  Worker	   *work;

};

#endif /* _MirrorFacetH_ */
