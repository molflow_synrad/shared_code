
#pragma once
#include "GLAppGui/GLWindow.h"
#include <memory>

#define MODE_RECTANGLE 0
#define MODE_CIRCLE 1
#define MODE_RACETRACK 2


class GLWindow;
class GLButton;
class GLTextField;
class GLTitledPanel;
class GLRadioGroup;
class GLRadioButton;
class GLLabel;
class GLIcon;

class InterfaceGeometry;
class Worker;

class CreateShape : public GLWindow {

public:

  // Construction
  CreateShape(InterfaceGeometry *interfGeom,Worker *work);

  // Implementation
  void ProcessMessage(GLComponent *src,int message) override;

private:

	void EnableDisableControls();

  	InterfaceGeometry     *interfGeom;
  	Worker	   *work;

	GLTitledPanel	*shapePanel;
	std::shared_ptr<GLRadioGroup> shapeRadioGroup;
	GLRadioButton* racetrackRadioButton;
	GLRadioButton* ellipseRadioButton;
	GLRadioButton* rectangleRadioButton;
	GLTitledPanel	*positionPanel;
	GLLabel	*normalStatusLabel;
	GLButton	*normalVertexButton;
	GLButton	*facetNormalButton;
	GLLabel	*axisStatusLabel;
	GLButton	*axisVertexButton;
	GLButton	*axisFacetUButton;
	GLLabel	*centerStatusLabel;
	GLButton	*centerVertexButton;
	GLButton	*facetCenterButton;
	GLLabel* normalLabel;
	GLLabel* normalXLabel;
	GLLabel* normalYLabel;
	GLLabel* normalZLabel;
	GLTextField	*normalXtext;
	GLTextField	*normalYtext;
	GLTextField	*normalZtext;
	GLLabel* axisDirLabel;
	GLLabel* axisDirXLabel;
	GLLabel* axisDirYLabel;
	GLLabel* axisDirZLabel;
	GLTextField* axisDirXtext;
	GLTextField* axisDirYtext;
	GLTextField* axisDirZtext;
	GLLabel* centerLabel;
	GLLabel* centerXLabel;
	GLLabel* centerYLabel;
	GLLabel* centerZLabel;
	GLTextField	*centerXtext;
	GLTextField	*centerYtext;
	GLTextField	*centerZtext;
	GLTitledPanel	*sizePanel;
	GLButton	*fullCircleButton;
	GLTextField* nbStepsText;
	GLLabel* nbStepsLabel;
	GLLabel* racetrackTopLengthLabel;
	GLTextField	*racetrackTopLengthText;
	GLLabel* racetrackTopCmLabel;
	GLLabel* axis2LengthLabel;
	GLTextField* axis2LengthText;
	GLLabel* aix2CmLabel;
	GLLabel* axis1LengthLabel;
	GLTextField* axis1LengthText;
	GLLabel* axis1CmLabel;
	GLButton	*createButton;

	GLIcon *rectangleIcon, *circleIcon, *racetrackIcon;
	size_t mode;
};