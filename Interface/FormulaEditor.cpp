#include "FormulaEditor.h"
#include "ConvergencePlotter.h"
#include "Formulas.h"
#include "GLAppGui/GLToolkit.h"
#include "GLAppGui/GLMessageBox.h"
#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLToggle.h"
#include "GLAppGui/GLTitledPanel.h"
#include "GLAppGui/GLList.h"
#include "Helper/MathTools.h"
#include "Helper/StringHelper.h"
#include "GLAppCore/GLStringHelper.h"
#include <sstream>
#include <algorithm>

#include "Worker.h"

#if defined(MOLFLOW)
#include "../../src/MolFlow.h"
extern MolFlow* mApp;
#endif

#if defined(SYNRAD)
#include "../src/SynRad.h"
extern SynRad* mApp;
#endif


extern char formulaSyntax[]; //Defined in MolFlow.cpp or SynRad.cpp

static const size_t nbCol = 3;
static const std::vector<std::string> columnTitles = { "Expression", "Name (optional)", "Value" };
static const std::vector<int> columnAlignments = { ALIGN_LEFT, ALIGN_LEFT, ALIGN_LEFT };
static const std::vector<int> columnEditModes = { EDIT_STRING, EDIT_STRING, EDIT_NONE };


FormulaEditor::FormulaEditor(Worker *w, std::shared_ptr<Formulas> formulas) : GLWindow() {
    columnRatios = { 0.333,0.333,0.333 };

    int wD = 500;
    int hD = 250;

    work = w;
    appFormulas = formulas;
    appFormulas->convergenceDataChanged = false;

    SetTitle("Formula Editor");
    SetIconfiable(true);
    SetResizable(true);

    panel1 = new GLTitledPanel("Formula list");
    Add(panel1);

    formulaList = new GLList(0);
    formulaList->SetColumnLabelVisible(true);
    formulaList->SetRowLabelVisible(true);
    formulaList->SetHScrollVisible(false);
    formulaList->SetGrid(true);
	formulaList->SetAutoRowLabel(true);
	formulaList->SetCopyColumNames(true);
    Add(formulaList);

    recalcButton = new GLButton(0, "Recalculate now");
    Add(recalcButton);

	moveUpButton = new GLButton(0, "Move Up");
    moveUpButton->SetEnabled(false);
    Add(moveUpButton);

    moveDownButton = new GLButton(0, "Move Down");
    moveDownButton->SetEnabled(false);
    Add(moveDownButton);

	autoUpdateToggle = new GLToggle(0, "Auto-update formulas");
	autoUpdateToggle->SetState(mApp->autoUpdateFormulas);
	Add(autoUpdateToggle);

    recordConvergenceToggle = new GLToggle(0, "Record convergence");
    recordConvergenceToggle->SetState(true);
    appFormulas->recordConvergence = recordConvergenceToggle->GetState();
    Add(recordConvergenceToggle);	

#ifdef MOLFLOW
	copyAllButton = new GLButton(0, "Copy table (all moments)");
	Add(copyAllButton);
#endif	

    convPlotterButton = new GLButton(0, "Open convergence plotter >>");
    Add(convPlotterButton);

    panel2 = new GLTitledPanel("Formula syntax");
    panel2->SetClosable(true);
	panel2->Close();
    Add(panel2);

	std::vector<std::string> formulaSyntaxHelpLines = GLApp_StringHelper::StringToMultiLines(formulaSyntax);

    formulaSyntaxHelp = new GLList(0);
	formulaSyntaxHelp->SetSize(1, formulaSyntaxHelpLines.size());
    formulaSyntaxHelp->SetGrid(false);
	formulaSyntaxHelp->SetAutoRowLabel(true);
	formulaSyntaxHelp->SetHScrollVisible(false);
    panel2->Add(formulaSyntaxHelp);

	for (size_t i = 0; i < formulaSyntaxHelpLines.size(); i++) {
		formulaSyntaxHelp->SetValueAt(0, i, formulaSyntaxHelpLines[i]);
	}

    // Place in top left corner
    //int wS, hS;
    //GLToolkit::GetWindowSize(&wS, &hS);
    int xD = /*(wS - wD - 215)*/ 10;
    int yD = 30;
    SetBounds(xD, yD, wD, hD);

    RestoreDeviceObjects();

}

void FormulaEditor::ProcessMessage(GLComponent *src, int message) {
	switch (message) {
	case MSG_PANELR:
	{
		if (src == panel2) {
			int x, y, w, h;
			GetBounds(&x, &y, &w, &h); //Current window size
			if (panel2->IsClosed()) {
				SetBounds(x, y, w, h - formulaSyntaxHeight); //Shrink window
				//formulaSyntaxHelp->SetVisible(false);
			}
			else {
				SetBounds(x, y, w, h + formulaSyntaxHeight); //Expand window
				//formulaSyntaxHelp->SetVisible(true);
			}
		}
		break;
	}
	case MSG_BUTTON:
		if (src == recalcButton) {
            appFormulas->EvaluateFormulas(work->globalStatCache.globalHits.nbDesorbed);
            UpdateValues();
		}
		else if (src == convPlotterButton) {
            if (!mApp->convergencePlotter) {
                mApp->convergencePlotter = new ConvergencePlotter(work, mApp->appFormulas);
            }
            if(!mApp->convergencePlotter->IsVisible()){
                mApp->convergencePlotter->Refresh();
                mApp->convergencePlotter->SetVisible(true);
            }
            else {
                mApp->convergencePlotter->SetVisible(false);
            }
        }
		else if (src == moveUpButton) {
			int selRow = formulaList->GetSelectedRow();
			if (selRow <= 0) {
				//Interface bug
				DEBUG_BREAK;
				return;
			}
			std::swap(appFormulas->formulas[selRow], appFormulas->formulas[selRow - 1]);
            std::swap(appFormulas->convergenceData[selRow], appFormulas->convergenceData[selRow - 1]);
            std::swap(appFormulas->formulaValueCache[selRow], appFormulas->formulaValueCache[selRow - 1]);
            formulaList->SetSelectedRow(selRow - 1);
			EnableDisableMoveButtons();
			Refresh();
		}
		else if (src == moveDownButton) {
			int selRow = formulaList->GetSelectedRow();
			if (selRow > appFormulas->formulas.size() - 2) {
				//Interface bug
				DEBUG_BREAK;
				return;
			}
			std::swap(appFormulas->formulas[selRow], appFormulas->formulas[selRow + 1]);
            std::swap(appFormulas->convergenceData[selRow], appFormulas->convergenceData[selRow + 1]);
            std::swap(appFormulas->formulaValueCache[selRow], appFormulas->formulaValueCache[selRow + 1]);
            formulaList->SetSelectedRow(selRow + 1);
			EnableDisableMoveButtons();
			Refresh();
		}
#ifdef MOLFLOW
		else if (src == copyAllButton) {
			std::string clipboardText = appFormulas->ExportFormulasAtAllMoments(work);
			SDL_SetClipboardText(clipboardText.c_str());
		}
#endif
		break;
    case MSG_TOGGLE:
        if (src == recordConvergenceToggle) {
            appFormulas->recordConvergence = recordConvergenceToggle->GetState();
		}
		else if (src == autoUpdateToggle) {
			mApp->autoUpdateFormulas = autoUpdateToggle->GetState();
		}
        break;
	case MSG_TEXT:
	case MSG_LIST:
	{
		for (size_t row = 0; row < (formulaList->GetNbRow() - 1); row++) { //regular lines

			if (formulaList->GetValueAt(0, row) != userExpressions[row]) { //Expression changed
				if (row >= appFormulas->formulas.size()) {
					//Interface bug
					DEBUG_BREAK;
					return;
				}
				if (!formulaList->GetValueAt(0, row).empty() || !formulaList->GetValueAt(1, row).empty()) //Name or expr. not empty
				{
                    appFormulas->formulas[row].SetExpression(formulaList->GetValueAt(0, row));
                    appFormulas->formulas[row].Parse();
					Refresh();
				}
				else
				{
                    appFormulas->formulas.erase(appFormulas->formulas.begin() + row);
                    appFormulas->UpdateVectorSize();
					Refresh();
				}
				EnableDisableMoveButtons();
				break;
			}

			if (formulaList->GetValueAt(1, row) != userFormulaNames[row]) { //Name changed
				if (!formulaList->GetValueAt(0, row).empty() || !formulaList->GetValueAt(1, row).empty()) //Name or expr. not empty
				{
					appFormulas->formulas[row].SetName(formulaList->GetValueAt(1, row));
					Refresh();
				}
				else
				{
					appFormulas->formulas.erase(appFormulas->formulas.begin() + row);
                    appFormulas->UpdateVectorSize();
                    Refresh();
				}
				EnableDisableMoveButtons();
				break;
			}

		}
		if (!formulaList->GetValueAt(0, formulaList->GetNbRow() - 1).empty()) { //last line expression enter
			//Add new line
			appFormulas->AddFormula("", formulaList->GetValueAt(0, formulaList->GetNbRow() - 1));
			Refresh();
		}
		else if (!formulaList->GetValueAt(1, formulaList->GetNbRow() - 1).empty()) { //last line name enter
			//Add new line
            appFormulas->AddFormula(formulaList->GetValueAt(1, formulaList->GetNbRow() - 1),"");
			Refresh();
		}
		EnableDisableMoveButtons();
		appFormulas->convergenceDataChanged = true;

        appFormulas->EvaluateFormulas(work->globalStatCache.globalHits.nbDesorbed);
        UpdateValues();

		break;
	}
	case MSG_LIST_COL: {
        int x, y, w, h;
        GetBounds(&x, &y, &w, &h);
        auto sum = (double) (w - 45);
        std::vector<double> colWidths(nbCol);
        for (size_t i = 0; i < nbCol; i++) {
            colWidths[i] = (double) formulaList->GetColWidth(i);
        }
        for (size_t i = 0; i < nbCol; i++) {
            columnRatios[i] = colWidths[i] / sum;
        }
        break;
    }
    default:
        break;
	}

	GLWindow::ProcessMessage(src, message);
}

void FormulaEditor::SetBounds(int x, int y, int w, int h) {
	int formulaHeight = (panel2->IsClosed() ? 0 : formulaSyntaxHeight);
	panel1->SetBounds(5, 5, w - 10, h - 150 - formulaHeight);
	formulaList->SetBounds(10, 22, w - 20, h - 165 - formulaHeight);
	for (size_t i=0;i<nbCol;i++) {
		formulaList->SetColumnWidth(i, (int)(columnRatios[i] * (double)(w - 45)));
	}
	const int buttonHeight = 20;
	recalcButton->SetBounds(10, h - 130 - formulaHeight, 150, buttonHeight);
	autoUpdateToggle->SetBounds(10, h - 105 - formulaHeight, 100, buttonHeight);
	moveUpButton->SetBounds(w - 150, h - 130 - formulaHeight, 65, buttonHeight);
	moveDownButton->SetBounds(w - 80, h - 130 - formulaHeight, 65, buttonHeight);
	
	recordConvergenceToggle->SetBounds(w-130, h - 105 - formulaHeight, 120, buttonHeight);
	convPlotterButton->SetBounds(w-200, h - 80 - formulaHeight, 190, buttonHeight);
    
#ifdef MOLFLOW
	copyAllButton->SetBounds(10, h - 80 - formulaHeight, 150, buttonHeight);
#endif

	panel2->SetBounds(5, h - 50 - formulaHeight, w - 10, 20 + formulaHeight);
	panel2->SetCompBounds(formulaSyntaxHelp, 10, 20, w-30, formulaHeight-10);
	formulaSyntaxHelp->SetColumnWidth(0, w-50);

	SetMinimumSize(500, 200 + formulaHeight);
	GLWindow::SetBounds(x, y, w, h);
}

void FormulaEditor::EnableDisableMoveButtons()
{
	int selRow = formulaList->GetSelectedRow(false);
	if (selRow == -1 || selRow >= ((int)(formulaList->GetNbRow()) - 1)) { //Nothing or very last (empty) row selected
		moveUpButton->SetEnabled(false);
		moveDownButton->SetEnabled(false);
	}
	else if (selRow == 0) { //First row
		moveUpButton->SetEnabled(false);
		moveDownButton->SetEnabled(true);
	}
	else if (selRow == ((int)(formulaList->GetNbRow()) - 2)) { //Last (formula) row
		moveUpButton->SetEnabled(true);
		moveDownButton->SetEnabled(false);
	}
	else {
		moveUpButton->SetEnabled(true);
		moveDownButton->SetEnabled(true);
	}
}

void FormulaEditor::RebuildList() {
	//Rebuild list based on locally stored userExpressions
	int x, y, w, h;
	GetBounds(&x, &y, &w, &h);
	formulaList->SetSize(nbCol, userExpressions.size() + 1);
	for (size_t i = 0; i<nbCol; i++)
		formulaList->SetColumnWidth(i, (int)(columnRatios[i] * (double)(w - 45)));
	formulaList->SetColumnLabels(columnTitles);
	formulaList->SetColumnAligns(columnAlignments);
	formulaList->SetColumnEditable(columnEditModes);

	for (size_t u = 0; u < userExpressions.size(); u++) {
		formulaList->SetValueAt(0, u, userExpressions[u]);
		formulaList->SetValueAt(1, u, userFormulaNames[u]);
	}
}

void FormulaEditor::Refresh() {
	//Load contents of window from global (interface/app) formulas
	size_t nbFormula = appFormulas->formulas.size();
	userExpressions.resize(nbFormula);
	userFormulaNames.resize(nbFormula);
	for (size_t i = 0; i < nbFormula; i++) {
		userExpressions[i] = appFormulas->formulas[i].GetExpression();
		userFormulaNames[i] = appFormulas->formulas[i].GetName();
	}
	RebuildList();
    appFormulas->convergenceDataChanged = true;
    UpdateValues();
}

void FormulaEditor::UpdateValues() {

	// This only displays formula values already evaluated in appFormulas
	// Therefore formulas should be updated beforehand by calling appFormulas->EvaluateFormulas
	
	for (size_t i = 0; i < appFormulas->formulas.size(); i++) {
		std::string valueString = appFormulas->GetFormulaValue(i);
		formulaList->SetValueAt(2, i, valueString);
#if defined(MOLFLOW)
		formulaList->SetColumnColor(2,work->displayedMoment == 0 ? COLOR_BLACK : COLOR_BLUE);
#endif
	}
}