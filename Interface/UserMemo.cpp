#include "UserMemo.hpp"

#include "GLAppGui/GLToolkit.h"
#include "GLAppGui/GLMessageBox.h"
#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLTextField.h"
#include "GLAppGui/GLList.h"
#include "Helper/StringHelper.h"
#include "GLAppCore/GLStringHelper.h"
#include "File.h"

#include "Geometry_shared.h"
#include "Worker.h"

#include "Interface.h"

UserMemo::UserMemo(Worker& appWorker, Interface& _iApp) : GLWindow(), worker(appWorker), iApp(_iApp) {
    
    int width = 480;
    int height = 400; //Height extended runtime when formula syntax panel is expanded

    SetTitle("File metadata");
    SetIconfiable(true);
    SetResizable(true);
    SetMinimumSize(450, 300);

    fileNameLabel=new GLLabel("File name:");
    Add(fileNameLabel);

    fileNameText = new GLTextField(0,"");
    fileNameText->SetEditable(false);
    Add(fileNameText);

    filePathButton = new GLButton(0,"File path:");
    Add(filePathButton);

    filePathText = new GLTextField(0, "");
    filePathText->SetEditable(false);
    Add(filePathText);

    firstSaveLabel = new GLLabel("First save:");
    Add(firstSaveLabel);

    firstSaveText = new GLTextField(0, "");
    firstSaveText->SetEditable(false);
    Add(firstSaveText);

    lastSaveLabel = new GLLabel("Last saved:");
    Add(lastSaveLabel);

    lastSaveText = new GLTextField(0, "");
    lastSaveText->SetEditable(false);
    Add(lastSaveText);

    authorLabel = new GLLabel("Author(s):");
    Add(authorLabel);

    authorText = new GLTextField(0, "");
    authorText->SetEditable(true);
    Add(authorText);

    memoLabel = new GLLabel("Memo:");
    Add(memoLabel);

    memoList = new GLList(0);
    memoList->SetSize(1, 6);
    
    std::vector<int>   fEdits = { EDIT_STRING };
    memoList->SetColumnEditable(fEdits);
    memoList->SetColumnWidthForAll(600);
    memoList->SetGrid(true);
    memoList->SetColumnLabelVisible(true);
    Add(memoList);

    insertLineButton = new GLButton(0, "Add line below");
    Add(insertLineButton);

    deleteLineButton = new GLButton(0, "Delete sel. line");
    Add(deleteLineButton);
    

    copyButton = new GLButton(0, "Copy memo");
    Add(copyButton);

    pasteButton = new GLButton(0, "Paste");
    Add(pasteButton);

    applyButton = new GLButton(0, "Apply");
    applyButton->SetEnabled(false);
    Add(applyButton);

    SetBounds(30, 30, width, height);
    RestoreDeviceObjects();
}

void UserMemo::SetBounds(int x, int y, int width, int height) {

    int cursorY = 10;
    int firstColX = 10;
    int firstColWidth = 50;
    int secondColWidth = width-firstColWidth-30;
    int secondColX = firstColX + firstColWidth + 10;
    int lineHeight = 20;
    int listHeight = height - 210;
    int buttonWidth = 80;

    fileNameLabel->SetBounds(firstColX, cursorY, firstColWidth, lineHeight);
    fileNameText->SetBounds(secondColX, cursorY, secondColWidth, lineHeight);
    cursorY += lineHeight + 5;

    filePathButton->SetBounds(firstColX, cursorY, firstColWidth, lineHeight);
    filePathText->SetBounds(secondColX, cursorY, secondColWidth, lineHeight);
    cursorY += lineHeight + 5;

    firstSaveLabel->SetBounds(firstColX, cursorY, firstColWidth, lineHeight);
    firstSaveText->SetBounds(secondColX, cursorY, secondColWidth, lineHeight);
    cursorY += lineHeight + 5;

    lastSaveLabel->SetBounds(firstColX, cursorY, firstColWidth, lineHeight);
    lastSaveText->SetBounds(secondColX, cursorY, secondColWidth, lineHeight);
    cursorY += lineHeight + 5;

    authorLabel->SetBounds(firstColX, cursorY, firstColWidth, lineHeight);
    authorText->SetBounds(secondColX, cursorY, secondColWidth, lineHeight);
    cursorY += lineHeight + 5;

    memoLabel->SetBounds(firstColX, cursorY, firstColWidth, lineHeight);
    cursorY += lineHeight + 5;

    memoList->SetBounds(firstColX, cursorY, width - 20, listHeight);
    cursorY += listHeight + 5;

    int cursorX = 10;

    insertLineButton->SetBounds(cursorX, cursorY, buttonWidth, lineHeight);
    cursorX += buttonWidth + 5;
    deleteLineButton->SetBounds(cursorX, cursorY, buttonWidth, lineHeight);
    cursorX += buttonWidth + 5;
    copyButton->SetBounds(cursorX, cursorY, buttonWidth, lineHeight);
    cursorX += buttonWidth + 5;
    pasteButton->SetBounds(cursorX, cursorY, buttonWidth, lineHeight);
    cursorX += buttonWidth + 5;
    applyButton->SetBounds(width - buttonWidth - 10, cursorY, buttonWidth, lineHeight);
    GLWindow::SetBounds(x, y, width, height);
}

void UserMemo::Update() {
    GeomMetadata& metadata = worker.GetGeometry()->metadata;
    UpdateFileMetadata();
    authorText->SetText(metadata.authors);
    std::vector<std::string> lines = GLApp_StringHelper::StringToMultiLines(metadata.userMemo);
    memoList->SetSize(1, std::clamp(lines.size(), (size_t)6, (size_t)100),true);

    lines.resize(memoList->GetNbRow()); //Fill rest with empty lines
    for (size_t i = 0; i < lines.size(); i++) {
        memoList->SetValueAt(0, i, lines[i]);
    }
    applyButton->SetEnabled(false);
}

void UserMemo::UpdateFileMetadata() { //Called on save
    GeomMetadata& metadata = worker.GetGeometry()->metadata;

    fileNameText->SetText(FileUtils::GetFilename(worker.fullFileName));
    filePathText->SetText(FileUtils::GetPath(worker.fullFileName));
    firstSaveText->SetText(metadata.firstSaved);

    std::string lastSaveString = metadata.lastSaved;
    if (!metadata.usedAppVersion.empty()) lastSaveString += " with " + metadata.usedAppVersion;
    lastSaveText->SetText(lastSaveString);
}

void UserMemo::Apply() {
    GeomMetadata& metadata = worker.GetGeometry()->metadata;
    metadata.authors = authorText->GetText();
    std::vector<std::string> lines(memoList->GetNbRow());
    for (size_t i = 0; i < lines.size(); i++) {
        lines[i] = memoList->GetValueAt(0, i);
    }
    metadata.userMemo = GLApp_StringHelper::MultiLinesToString(lines);
    iApp.changedSinceSave = true;
    applyButton->SetEnabled(false);
}

void UserMemo::ProcessMessage(GLComponent* src, int message) {
    switch (message) {
    case MSG_BUTTON:
        if (src == applyButton) {
            Apply();
        }
        else if (src == filePathButton) {
            FileUtils::openFolder(FileUtils::GetPath(worker.fullFileName));
        }
        else if (src == pasteButton) {
            int currentLine = memoList->GetSelectedRow();
            if (currentLine == -1) currentLine = 0; //Paste from beginning if nothing is selected
            size_t offset = static_cast<size_t>(currentLine);
            std::vector<std::string> lines = GLApp_StringHelper::StringToMultiLines(SDL_GetClipboardText());
            size_t minRows = currentLine + lines.size(); //So content fits
            memoList->SetSize(1, std::clamp(memoList->GetNbRow(),currentLine + lines.size(), (size_t)100),true);
            for (size_t i = 0; i < lines.size(); ++i) {
                memoList->SetValueAt(0, i + offset, lines[i]);
            }
            applyButton->SetEnabled(true);
        }
        else if (src == copyButton) {
            std::vector<std::string> lines(memoList->GetNbRow());
            for (size_t i = 0; i < lines.size(); i++) {
                lines[i] = memoList->GetValueAt(0, i);
            }
            SDL_SetClipboardText(GLApp_StringHelper::MultiLinesToString(lines).c_str());
        }
        else if (src == deleteLineButton) {
            int currentLine = memoList->GetSelectedRow();
            size_t nbRow = memoList->GetNbRow();
            if (currentLine == -1 || currentLine >= nbRow) return; //Selected row is -1 if nothing's selected, and upon deletion, selected line can go out of valid lines.
            if (nbRow <= 1) return; //Setting a 0x0 size is not allowed, so first line can't be deleted
            std::vector<std::string> lines(nbRow);
            for (size_t i = 0; i < lines.size(); i++) {
                lines[i] = memoList->GetValueAt(0, i);
            }
            lines.erase(lines.begin() + currentLine);
            memoList->SetSize(1, lines.size(), true);
            for (size_t i = currentLine; i < lines.size(); ++i) {
                memoList->SetValueAt(0, i, lines[i]);
            }
            applyButton->SetEnabled(true);
        }
        else if (src == insertLineButton) {
            int currentLine = memoList->GetSelectedRow();
            size_t nbRow = memoList->GetNbRow();
            if (currentLine == -1) currentLine = nbRow-1; //Selected row is -1 if nothing's selected, and upon deletion, selected line can go out of valid lines.
            if (nbRow >= 100) return; //Max 100 lines
            std::vector<std::string> lines(nbRow);
            for (size_t i = 0; i < lines.size(); i++) {
                lines[i] = memoList->GetValueAt(0, i);
            }
            lines.insert(lines.begin() + currentLine+1,"");
            memoList->SetSize(1, lines.size(), true);
            for (size_t i = currentLine; i < lines.size(); ++i) {
                memoList->SetValueAt(0, i, lines[i]);
            }
            applyButton->SetEnabled(true);
        }
        break;

    case MSG_TEXT_UPD:
        applyButton->SetEnabled(true);
        break;
    default:
        break;
    }
    GLWindow::ProcessMessage(src, message);
}