
#define XYMODE 0
#define XZMODE 1
#define YZMODE 2
#define FACETNMODE 3
#define ABCDMODE 5

#include "MirrorVertex.h"
#include "Facet_shared.h"
#include "GLAppGui/GLTitledPanel.h"
#include "GLAppGui/GLToolkit.h"
#include "GLAppGui/GLWindowManager.h"
#include "GLAppGui/GLMessageBox.h"

#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLTextField.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLRadioGroup.h"
#include "GLAppGui/GLRadioButton.h"
#include "GLAppGui/GLTitledPanel.h"

#if defined(MOLFLOW)
#include "../../src/MolFlow.h"
#endif

#if defined(SYNRAD)
#include "../src/SynRad.h"
#endif

extern GLApplication* theApp;

#if defined(MOLFLOW)
extern MolFlow* mApp;
#endif

#if defined(SYNRAD)
extern SynRad* mApp;
#endif

MirrorVertex::MirrorVertex(InterfaceGeometry* g, Worker* w) :GLWindow() {

	int wD = 360;
	int hD = 250;

	SetTitle("Mirror/Project selected vertices");

	iPanel = new GLTitledPanel("Plane definiton mode");
	iPanel->SetBounds(5, 5, wD - 10, 165);
	Add(iPanel);

	planeDefinitionRadioGroup = std::make_shared<GLRadioGroup>();

	xyPlaneRadioButton = new GLRadioButton(0, "XY plane", planeDefinitionRadioGroup);
	xyPlaneRadioButton->SetBounds(10, 20, 100, 18);
	iPanel->Add(xyPlaneRadioButton);

	yzPlaneRadioButton = new GLRadioButton(0, "YZ plane", planeDefinitionRadioGroup);
	yzPlaneRadioButton->SetBounds(10, 45, 100, 18);
	iPanel->Add(yzPlaneRadioButton);

	xzPlaneRadioButton = new GLRadioButton(0, "XZ plane", planeDefinitionRadioGroup);
	xzPlaneRadioButton->SetBounds(10, 70, 100, 18);
	iPanel->Add(xzPlaneRadioButton);

	facetPlaneRadioButton = new GLRadioButton(0, "Plane of selected facet", planeDefinitionRadioGroup);
	facetPlaneRadioButton->SetBounds(10, 95, 100, 18);
	iPanel->Add(facetPlaneRadioButton);

	planeEquationRadioButton = new GLRadioButton(0, "Plane equation:", planeDefinitionRadioGroup);
	planeEquationRadioButton->SetBounds(10, 120, 60, 18);
	iPanel->Add(planeEquationRadioButton);

	getPlaneButton = new GLButton(0, "<-Get from 3 sel. vertex");
	getPlaneButton->SetBounds(110, 120, 120, 18);
	iPanel->Add(getPlaneButton);

	int planeTextW = 61;
	int planeLabelW = 20;
	int planeCursorX = 12;
	int planeMarginX = 2;

	aText = new GLTextField(0, "0");
	aText->SetBounds(planeCursorX, 145, planeTextW, 18);
	aText->SetEditable(false);
	iPanel->Add(aText);
	planeCursorX += planeTextW + planeMarginX;

	aLabel = new GLLabel("*X +");
	aLabel->SetBounds(planeCursorX, 145, planeLabelW, 18);
	iPanel->Add(aLabel);
	planeCursorX += planeLabelW + planeMarginX;

	bText = new GLTextField(0, "0");
	bText->SetBounds(planeCursorX, 145, planeTextW, 18);
	bText->SetEditable(false);
	iPanel->Add(bText);
	planeCursorX += planeTextW + planeMarginX;

	bLabel = new GLLabel("*Y +");
	bLabel->SetBounds(planeCursorX, 145, planeLabelW, 18);
	iPanel->Add(bLabel);
	planeCursorX += planeLabelW + planeMarginX;

	cText = new GLTextField(0, "0");
	cText->SetBounds(planeCursorX, 145, planeTextW, 18);
	cText->SetEditable(false);
	iPanel->Add(cText);
	planeCursorX += planeTextW + planeMarginX;

	cLabel = new GLLabel("*Z +");
	cLabel->SetBounds(planeCursorX, 145, planeLabelW, 18);
	iPanel->Add(cLabel);
	planeCursorX += planeLabelW + planeMarginX;

	dText = new GLTextField(0, "0");
	dText->SetBounds(planeCursorX, 145, planeTextW, 18);
	dText->SetEditable(false);
	iPanel->Add(dText);
	planeCursorX += planeTextW + planeMarginX;

	dLabel = new GLLabel("= 0");
	dLabel->SetBounds(planeCursorX, 145, planeLabelW, 18);
	iPanel->Add(dLabel);
	planeCursorX += planeLabelW + planeMarginX;

	int buttonW = 100;
	int buttonMarginX = 5;
	int buttonCursorX = wD / 2 - buttonW - buttonMarginX - buttonW / 2;

	mirrorButton = new GLButton(0, "Mirror vertex");
	mirrorButton->SetBounds(buttonCursorX, hD - 72, buttonW, 21);
	Add(mirrorButton);
	buttonCursorX += buttonW + buttonMarginX;

	mirrorCopyButton = new GLButton(0, "Copy mirror vertex");
	mirrorCopyButton->SetBounds(buttonCursorX, hD - 72, buttonW, 21);
	Add(mirrorCopyButton);
	buttonCursorX -= buttonW + buttonMarginX;

	projectButton = new GLButton(0, "Project vertex");
	projectButton->SetBounds(buttonCursorX, hD - 45, buttonW, 21);
	Add(projectButton);
	buttonCursorX += buttonW + buttonMarginX;

	projectCopyButton = new GLButton(0, "Copy project vertex");
	projectCopyButton->SetBounds(buttonCursorX, hD - 45, buttonW, 21);
	Add(projectCopyButton);
	buttonCursorX += buttonW + buttonMarginX;

	undoProjectButton = new GLButton(0, "Undo projection");
	undoProjectButton->SetBounds(buttonCursorX, hD - 45, buttonW, 21);
	undoProjectButton->SetEnabled(false);
	Add(undoProjectButton);

	// Center dialog
	int wS, hS;
	GLToolkit::GetWindowSize(&wS, &hS);
	int xD = (wS - wD) / 2;
	int yD = (hS - hD) / 2;
	SetBounds(xD, yD, wD, hD);

	RestoreDeviceObjects();

	interfGeom = g;
	work = w;
	planeMode = -1;
}

void MirrorVertex::ClearUndoVertices() {
	undoPoints.clear();
	undoProjectButton->SetEnabled(false);
}

void MirrorVertex::ProcessMessage(GLComponent* src, int message) {
	double a, b, c, d;

	switch (message) {

	case MSG_RADIO:
		OnRadioUpdate(src);
		break;

	case MSG_BUTTON:

		if (src == mirrorButton || src == mirrorCopyButton || src == projectButton || src == projectCopyButton) {
			if (interfGeom->GetNbSelectedVertex() == 0) {
				GLMessageBox::Display("No vertices selected", "Nothing to mirror", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}
			//Calculate the plane
			Vector3d P0, N;
			switch (planeMode) {
			case XYMODE:
				P0.x = 0.0; P0.y = 0.0; P0.z = 0.0;
				N.x = 0.0; N.y = 0.0; N.z = 1.0;
				break;
			case XZMODE:
				P0.x = 0.0; P0.y = 0.0; P0.z = 0.0;
				N.x = 0.0; N.y = 1.0; N.z = 0.0;
				break;
			case YZMODE:
				P0.x = 0.0; P0.y = 0.0; P0.z = 0.0;
				N.x = 1.0; N.y = 0.0; N.z = 0.0;
				break;
			case FACETNMODE:
			{
				if (interfGeom->GetNbSelectedFacets() != 1) {
					GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				int selFacetId = -1;
				for (int i = 0; selFacetId == -1 && i < interfGeom->GetNbFacet(); i++) {
					if (interfGeom->GetFacet(i)->selected) {
						selFacetId = i;
					}
				}
				P0 = *interfGeom->GetVertex(interfGeom->GetFacet(selFacetId)->indices[0]);
				N = interfGeom->GetFacet(selFacetId)->sh.N;
				break;
			}
			case ABCDMODE:

				if (!(aText->GetNumber(&a))) {
					GLMessageBox::Display("Invalid A coefficient", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(bText->GetNumber(&b))) {
					GLMessageBox::Display("Invalid B coefficient", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(cText->GetNumber(&c))) {
					GLMessageBox::Display("Invalid C coefficient", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(dText->GetNumber(&d))) {
					GLMessageBox::Display("Invalid D coefficient", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if ((a == 0.0) && (b == 0.0) && (c == 0.0) && (d == 0.0)) {
					GLMessageBox::Display("A, B, C are all zero. That's not a plane.", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				N.x = a; N.y = b; N.z = c;
				P0.x = 0.0; P0.y = 0; P0.z = 0;
				if (a != 0) P0.x = -d / a;
				else if (b != 0) P0.y = -d / b;
				else if (c != 0) P0.z = -d / c;
				break;

			default:
				GLMessageBox::Display("Select a plane definition mode.", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			if (mApp->AskToReset()) {
				undoPoints = interfGeom->MirrorProjectSelectedVertices(P0, N,
					(src == projectButton) || (src == projectCopyButton),
					(src == mirrorCopyButton) || (src == projectCopyButton), work);
				undoProjectButton->SetEnabled(src == projectButton);
				//theApp->UpdateModelParams();
				work->MarkToReload();
				mApp->UpdateFacetlistSelected();
				mApp->UpdateViewers();
				//GLWindowManager::FullRepaint();
			}
		}
		else if (src == getPlaneButton) {
			if (interfGeom->GetNbSelectedVertex() != 3) {
				GLMessageBox::Display("Select exactly three vertices.\nThey will define the mirroring plane.", "Error", GLDLG_OK, GLDLG_ICONINFO);
				return;
			}
			int v1Id = -1;
			int v2Id = -1;
			int v3Id = -1;

			for (int i = 0; v3Id == -1 && i < interfGeom->GetNbVertex(); i++) {
				if (interfGeom->GetVertex(i)->selected) {
					if (v1Id == -1) v1Id = i;
					else if (v2Id == -1) v2Id = i;
					else v3Id = i;
				}
			}

			Vector3d U2, V2, N2;
			U2 = (*(interfGeom->GetVertex(v1Id)) - *(interfGeom->GetVertex(v2Id))).Normalized();
			V2 = (*(interfGeom->GetVertex(v1Id)) - *(interfGeom->GetVertex(v3Id))).Normalized();
			N2 = CrossProduct(V2, U2);
			double nN2 = N2.Norme();
			if (nN2 < 1e-8) {
				GLMessageBox::Display("The 3 selected vertices are on a line.", "Can't define plane", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}
			N2 = 1.0 / nN2 * N2; // Normalize N2
			double a = N2.x;
			double b = N2.y;
			double c = N2.z;
			double d = -(Dot(N2, *(interfGeom->GetVertex(v1Id))));
			aText->SetText(a);
			bText->SetText(b);
			cText->SetText(c);
			dText->SetText(d);
			planeDefinitionRadioGroup->Select(planeEquationRadioButton);
			OnRadioUpdate(planeEquationRadioButton);
		}
		else if (src == undoProjectButton) {
			if (!mApp->AskToReset(work)) return;
			for (UndoPoint oriPoint : undoPoints) {
				if (oriPoint.oriId < interfGeom->GetNbVertex()) interfGeom->GetVertex(oriPoint.oriId)->SetLocation(oriPoint.oriPos);
			}
			undoProjectButton->SetEnabled(false);
			interfGeom->InitializeGeometry();
			//for(int i=0;i<nbSelected;i++)
			 //	interfGeom->SetFacetTexture(selection[i],interfGeom->GetFacet(selection[i])->tRatio,interfGeom->GetFacet(selection[i])->hasMesh);	
			work->MarkToReload();
			mApp->UpdateFacetlistSelected();
			mApp->UpdateViewers();
		}
		break;
	}

	GLWindow::ProcessMessage(src, message);
}

void MirrorVertex::OnRadioUpdate(GLComponent* src) {
	aText->SetEditable(src == planeEquationRadioButton);
	bText->SetEditable(src == planeEquationRadioButton);
	cText->SetEditable(src == planeEquationRadioButton);
	dText->SetEditable(src == planeEquationRadioButton);

	if (src == xyPlaneRadioButton) planeMode = XYMODE;
	if (src == yzPlaneRadioButton) planeMode = YZMODE;
	if (src == xzPlaneRadioButton) planeMode = XZMODE;
	if (src == facetPlaneRadioButton) planeMode = FACETNMODE;
	if (src == planeEquationRadioButton) planeMode = ABCDMODE;

}