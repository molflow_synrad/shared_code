/*
  File:        ScaleVertex.h
  Description: Mirror facet to plane dialog
*/

#ifndef _ScaleVertexH_
#define _ScaleVertexH_

#include "GLAppGui/GLWindow.h"
#include <memory>

class GLButton;
class GLTextField;
class GLLabel;
class GLRadioGroup;
class GLRadioButton;
class GLTitledPanel;

class InterfaceGeometry;
class Worker;

class ScaleVertex : public GLWindow {

public:
  // Construction
  ScaleVertex(InterfaceGeometry *interfGeom,Worker *work);
  void ProcessMessage(GLComponent *src,int message) override;

  // Implementation
private:

  void OnRadioUpdate(GLComponent *src);
  
  GLTitledPanel *iPanel;
  GLTitledPanel *sPanel;
  GLButton     *scaleButton;
  GLButton    *copyButton;
  GLButton    *getSelVertexButton;
  std::shared_ptr<GLRadioGroup> invariantRadioGroup;
  GLRadioButton* xzyRadioButton;
  GLRadioButton* selectedVertexRadioButton;
  GLRadioButton* centerFacetRadioButton;
  GLTextField *xText;
  GLTextField *yText;
  GLTextField *zText;
  GLTextField *vertexNumber;
  GLTextField *factorNumber;
  GLTextField *OnePerFactor;
  GLTextField *factorNumberX;
  GLTextField *factorNumberY;
  GLTextField *factorNumberZ;
  std::shared_ptr<GLRadioGroup> scaleFactorRadioGroup;
  GLRadioButton* uniform;
  GLRadioButton* distort;

  int nbFacetS, invariantMode, scaleMode;

  InterfaceGeometry     *interfGeom;
  Worker	   *work;
};

#endif /* _ScaleVertexH_ */
