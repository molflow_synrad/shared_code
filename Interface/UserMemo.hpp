#pragma once

#include "GLAppGui/GLWindow.h"
class Interface; //changedSinceSave
class Worker;
class GLButton;
class GLLabel;
class GLTextField;
class GLToggle;
class GLTitledPanel;
class GLList;
class Worker;
struct Formulas;

#include <vector>
#include <string>
#include <memory>

class UserMemo : public GLWindow {

public:

    // Construction
    UserMemo(Worker& worker, Interface& _iApp);

    void Update();
    void UpdateFileMetadata();
    // Implementation



private:
    void SetBounds(int x, int y, int width, int height) override;
    void ProcessMessage(GLComponent* src, int message) override;
    void Apply();
    Interface& iApp;
    Worker& worker;

    GLLabel* fileNameLabel,  * firstSaveLabel, * lastSaveLabel, * authorLabel, * memoLabel;
    GLTextField* fileNameText, * filePathText, * firstSaveText, * lastSaveText, * authorText;
    GLButton* applyButton, *filePathButton, *copyButton, *pasteButton, *insertLineButton, *deleteLineButton;
    GLList* memoList;

};