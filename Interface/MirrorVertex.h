

/*
  File:        MirrorVertex.h
  Description: Mirror vertex to plane dialog
*/
#ifndef _MirrorVertexH_
#define _MirrorVertexH_

#include "GLAppGui/GLWindow.h"
#include "Geometry_shared.h" //UndoPoint
#include <vector>
#include <memory>

class InterfaceGeometry;
class Worker;
class GLButton;
class GLTextField;
class GLLabel;
class GLRadioGroup;
class GLRadioButton;
class GLTitledPanel;

class MirrorVertex : public GLWindow {

public:
  // Construction
  MirrorVertex(InterfaceGeometry *interfGeom,Worker *work);
  void ClearUndoVertices();
  void ProcessMessage(GLComponent *src,int message) override;

  // Implementation
private:

  void OnRadioUpdate(GLComponent *src);
  
  GLTitledPanel *iPanel;
  GLButton     *mirrorButton, *mirrorCopyButton;
  GLButton    *projectButton, *projectCopyButton, *undoProjectButton;
  GLButton	  *getPlaneButton;
  std::shared_ptr<GLRadioGroup> planeDefinitionRadioGroup;
  GLRadioButton* xyPlaneRadioButton;
  GLRadioButton* yzPlaneRadioButton;
  GLRadioButton* xzPlaneRadioButton;
  GLRadioButton* facetPlaneRadioButton;
  GLRadioButton* planeEquationRadioButton;
  GLTextField *aText;
  GLTextField *bText;
  GLTextField *cText;
  GLTextField *dText;
  GLLabel		*aLabel;
  GLLabel		*bLabel;
  GLLabel		*cLabel;
  GLLabel		*dLabel;

  int nbFacetS;
  int    planeMode;
  std::vector<UndoPoint> undoPoints;

  InterfaceGeometry     *interfGeom;
  Worker	   *work;

};

#endif /* _MirrorVertexH_ */
