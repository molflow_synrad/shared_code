

/*
File:        RotateVertex.h
Description: Rotate vertex around axis dialog
*/
#pragma once

#include "GLAppGui/GLWindow.h"
#include <memory>

class GLButton;
class GLTextField;
class GLLabel;
class GLRadioGroup;
class GLRadioButton;
class GLTitledPanel;

class InterfaceGeometry;
class Worker;

class RotateVertex : public GLWindow {

public:
	// Construction
	RotateVertex(InterfaceGeometry *interfGeom, Worker *work);
	void ProcessMessage(GLComponent *src, int message);

	// Implementation
private:

	void OnRadioUpdate(GLComponent *src);

	GLTitledPanel *iPanel;
	GLButton     *moveButton, *copyButton, *getBaseVertexButton, *getDirVertexButton;
  	std::shared_ptr<GLRadioGroup> axisDefinitionRadioGroup;
	GLRadioButton* axisXRadioButton;
	GLRadioButton* axisYRadioButton;
	GLRadioButton* axisZRadioButton;
	GLRadioButton* uVectorRadioButton;
	GLRadioButton* vVectorRadioButton;
	GLRadioButton* normalVectorRadioButton;
	GLLabel     *lNum;
	GLRadioButton* equationRadioButton;
	GLTextField *aText;
	GLTextField *bText;
	GLTextField *cText;
	GLTextField *uText;
	GLTextField *vText;
	GLTextField *wText;
	GLTextField *degText, *radText;
	GLLabel		*aLabel;
	GLLabel		*bLabel;
	GLLabel		*cLabel;
	GLLabel		*uLabel;
	GLLabel		*vLabel;
	GLLabel		*wLabel;
	GLLabel		*degLabel, *radLabel;

	int nbFacetS;
	int    axisMode;

	InterfaceGeometry   *interfGeom;
	Worker	   *work;

};

