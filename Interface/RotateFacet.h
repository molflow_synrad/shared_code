

/*
  File:        RotateFacet.h
  Description: Rotate facet around axis dialog
*/
#pragma once

#include "GLAppGui/GLWindow.h"
#include <memory>

class GLButton;
class GLTextField;
class GLLabel;
class GLRadioGroup;
class GLRadioButton;
class GLTitledPanel;

class InterfaceGeometry;
class Worker;

class RotateFacet : public GLWindow {

public:
  // Construction
  RotateFacet(InterfaceGeometry *interfGeom,Worker *work);
  void ProcessMessage(GLComponent *src,int message) override;

  // Implementation
private:

  void OnRadioUpdate(GLComponent *src);
  
  GLTitledPanel *iPanel;
  GLButton     *moveButton,*copyButton,*getSelFacetButton,*getBaseVertexButton,*getDirVertexButton;
  std::shared_ptr<GLRadioGroup> axisDefinitionRadioGroup;
  GLRadioButton* axisXRadioButton;
  GLRadioButton* axisYRadioButton;
  GLRadioButton* axisZRadioButton;
  GLRadioButton* uVectorRadioButton;
  GLRadioButton* vVectorRadioButton;
  GLRadioButton* normalVectorRadioButton;
  GLLabel     *lNum;
  GLRadioButton* selectedVerticesRadioButton;
  GLRadioButton* equationRadioButton;
  GLTextField *aText;
  GLTextField *bText;
  GLTextField *cText;
  GLTextField *uText;
  GLTextField *vText;
  GLTextField *wText;
  GLTextField *facetNumber;
  GLTextField *degText,*radText;
  GLLabel		*aLabel;
  GLLabel		*bLabel;
  GLLabel		*cLabel;
  GLLabel		*uLabel;
  GLLabel		*vLabel;
  GLLabel		*wLabel;
  GLLabel		*degLabel,*radLabel;

  int nbFacetS;
  int    axisMode;

  InterfaceGeometry     *interfGeom;
  Worker	   *work;

};

