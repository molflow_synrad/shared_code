#pragma once

#include "GLAppGui/GLWindow.h"
#include "GLAppCore/GLFormula.h"
class GLButton;
class GLLabel;
class GLTextField;
class GLToggle;
class GLTitledPanel;
class GLList;
class Worker;
struct Formulas;

#include <vector>
#include <string>
#include <memory>

class FormulaEditor : public GLWindow {

public:

  // Construction
  FormulaEditor(Worker *w, std::shared_ptr<Formulas> formulas);

  void RebuildList();
  void Refresh();
  void UpdateValues();

  // Implementation
  void ProcessMessage(GLComponent *src,int message) override;

  void SetBounds(int x, int y, int w, int h) override;
  

private:

  int formulaSyntaxHeight = 200; //Expand window by this much for syntax help

  Worker	   *work;

  GLToggle  *recordConvergenceToggle;
  GLToggle* autoUpdateToggle;
  GLButton    *recalcButton;
    GLButton    *convPlotterButton;
    GLButton		*moveUpButton;
  GLButton		*moveDownButton;
   GLList      *formulaList;
   GLList     *formulaSyntaxHelp;

  GLTitledPanel *panel1;
  GLTitledPanel *panel2;

#ifdef MOLFLOW
  GLButton* copyAllButton;
#endif

  std::vector<std::string> userExpressions,userFormulaNames;
  std::vector<double> columnRatios;

  void EnableDisableMoveButtons();

public:
    std::shared_ptr<Formulas> appFormulas;
};
