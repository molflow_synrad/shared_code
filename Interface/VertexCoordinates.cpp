#include "VertexCoordinates.h"
#include "GLAppGui/GLTitledPanel.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLToolkit.h"
#include "GLAppGui/GLWindowManager.h"
#include "GLAppGui/GLMessageBox.h"
#include "GLAppGui/GLTextField.h"
#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLList.h"

#include "Geometry_shared.h"

#if defined(MOLFLOW)
#include "../../src/MolFlow.h"
#endif

#if defined(SYNRAD)
#include "../src/SynRad.h"
#endif
#include "GLAppGui/GLInputBox.h"

#if defined(MOLFLOW)
extern MolFlow *mApp;
#endif

#if defined(SYNRAD)
extern SynRad*mApp;
#endif

static const std::vector<int> flWidth = {40, 100, 100, 100};
static const std::vector<std::string> flName = {"Vertex#", "X", "Y", "Z"};
static const std::vector<int> flAligns = {ALIGN_LEFT, ALIGN_LEFT, ALIGN_LEFT, ALIGN_LEFT};
static const std::vector<int> fEdits = {0, EDIT_NUMBER, EDIT_NUMBER, EDIT_NUMBER};

VertexCoordinates::VertexCoordinates():GLWindow() {

  int wD = 405;
  int hD = 350;
  SetIconfiable(true);
  SetResizable(false);

  setXbutton = new GLButton(0, "X");
  setXbutton->SetBounds(5, hD - 43, 16, 19);
  Add(setXbutton);
  setYbutton = new GLButton(0, "Y");
  setYbutton->SetBounds(27, hD - 43, 16, 19);
  Add(setYbutton);
  setZbutton = new GLButton(0, "Z");
  setZbutton->SetBounds(49, hD - 43, 16, 19);
  Add(setZbutton);

  refreshButton = new GLButton(0,"Refresh from geometry");
  refreshButton->SetBounds(wD-250,hD-43,120,19);
  Add(refreshButton);
  
  applyButton = new GLButton(0,"Apply to geometry");
  applyButton->SetBounds(wD-125,hD-43,120,19);
  Add(applyButton);

  vertexListC = new GLList(0);
  vertexListC->SetBounds(5,5,wD-10,hD-60);
  vertexListC->SetColumnLabelVisible(true);
  vertexListC->SetGrid(true);
  Add(vertexListC);

  // Center dialog
  int wS,hS;
  GLToolkit::GetWindowSize(&wS,&hS);
  int xD = (wS-wD)/2;
  int yD = (hS-hD)/2;
  SetBounds(xD,yD,wD,hD);

  RestoreDeviceObjects();

}

void VertexCoordinates::UpdateFromSelection() {

  char tmp[256];
  if(!IsVisible()) return;

  InterfaceGeometry *s = worker->GetGeometry();
  int count=0;
  vertexListC->SetSize(4,s->GetNbSelectedVertex());
  vertexListC->SetColumnWidths(flWidth);
  vertexListC->SetColumnLabels(flName);
  vertexListC->SetColumnAligns(flAligns);
  vertexListC->SetColumnEditable(fEdits);
  for(size_t i = 0; i < s->GetNbVertex(); i++) {
      if(s->GetVertex(i)->selected) {
          vertexListC->SetValueAt(0, count, fmt::format("{}", i + 1));
          Vector3d *v = s->GetVertex(i);
          vertexListC->SetValueAt(1, count, fmt::format("{:.10g}", v->x));
          vertexListC->SetValueAt(2, count, fmt::format("{:.10g}", v->y));
          vertexListC->SetValueAt(3, count, fmt::format("{:.10g}", v->z));
          count++;
      }
  }
}

void VertexCoordinates::Display(Worker *w) {
  SetTitle("Vertex coordinates");
  worker = w;
  SetVisible(true);
  UpdateFromSelection();

}

void VertexCoordinates::ProcessMessage(GLComponent *src,int message) {

  InterfaceGeometry *interfGeom = worker->GetGeometry();
  switch(message) {
    case MSG_BUTTON:
	  if (src == setXbutton) {
		  double coordValue;
		  char *coord = GLInputBox::GetInput("0", "New coordinate:", "Set all X coordinates to:");
		  if (!coord) return;
		  if (!sscanf(coord, "%lf", &coordValue)) {
			  GLMessageBox::Display("Invalid number", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  for (int i = 0; i < vertexListC->GetNbRow(); i++) {
			  vertexListC->SetValueAt(1, i, coord);
		  }
	  }
	  else if (src == setYbutton) {
		  double coordValue;
		  char *coord = GLInputBox::GetInput("0", "New coordinate:", "Set all Y coordinates to:");
		  if (!coord) return;
		  if (!sscanf(coord, "%lf", &coordValue)) {
			  GLMessageBox::Display("Invalid number", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  for (int i = 0; i < vertexListC->GetNbRow(); i++) {
			  vertexListC->SetValueAt(2, i, coord);
		  }
	  }
	  else if (src == setZbutton) {
		  double coordValue;
		  char *coord = GLInputBox::GetInput("0", "New coordinate:", "Set all Z coordinates to:");
		  if (!coord) return;
		  if (!sscanf(coord, "%lf", &coordValue)) {
			  GLMessageBox::Display("Invalid number", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  for (int i = 0; i < vertexListC->GetNbRow(); i++) {
			  vertexListC->SetValueAt(3, i, coord);
		  }
	  }
	  else if (src == applyButton) {
        int rep = GLMessageBox::Display("Apply geometry changes ?","Question",GLDLG_OK|GLDLG_CANCEL,GLDLG_ICONWARNING);
        if( rep == GLDLG_OK ) {
			if (mApp->AskToReset(worker)) {
			//if (worker->IsRunning()) worker->Stop_Public();
			mApp->changedSinceSave=true;
			for(size_t i=0;i<vertexListC->GetNbRow();i++) {
				double x,y,z;
				size_t id;
				id=vertexListC->GetValueInt(i,0)-1;
				if (!(id >= 0 && id < interfGeom->GetNbVertex())) { //wrong coordinates at row
					GLMessageBox::Display(fmt::format("Invalid vertex id in row {}\n Vertex {} doesn't exist.", i + 1, id + 1), "Incorrect vertex id", GLDLG_OK, GLDLG_ICONWARNING);
					return;
				}
				std::string xStr = vertexListC->GetValueAt(1, i);
				std::string yStr = vertexListC->GetValueAt(2, i);
				std::string zStr = vertexListC->GetValueAt(3, i);
				bool success = (1 == sscanf(xStr.c_str(), "%lf", &x));
				success = success && (1 == sscanf(yStr.c_str(), "%lf", &y));
				success = success && (1 == sscanf(zStr.c_str(), "%lf", &z));
				if (!success) { //wrong coordinates at row
					GLMessageBox::Display(fmt::format("Invalid coordinates in row {}", i + 1), "Incorrect vertex", GLDLG_OK, GLDLG_ICONWARNING);
					return;	
				}
				interfGeom->MoveVertexTo(id,x,y,z);
			}
			interfGeom->Rebuild();
          // Send to sub process
          worker->MarkToReload();
		 }
        }
      } else if (src == refreshButton) {
		  UpdateFromSelection();
	  }
    break;

  }

  GLWindow::ProcessMessage(src,message);

}
