
#include "MoveFacet.h"
#include "GLAppGui/GLTitledPanel.h"
#include "GLAppGui/GLToolkit.h"
#include "GLAppGui/GLWindowManager.h"
#include "GLAppGui/GLMessageBox.h"

#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLTextField.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLRadioGroup.h"
#include "GLAppGui/GLRadioButton.h"
#include "GLAppGui/GLToggle.h"

#include "Geometry_shared.h"
#include "Facet_shared.h"

#if defined(MOLFLOW)
#include "../../src/MolFlow.h"
#endif

#if defined(SYNRAD)
#include "../src/SynRad.h"
#include "Region_full.h"
#include "Helper/MathTools.h"
#endif

#if defined(MOLFLOW)
extern MolFlow *mApp;
#endif

#if defined(SYNRAD)
extern SynRad*mApp;
#endif
  
MoveFacet::MoveFacet(InterfaceGeometry *g,Worker *w):GLWindow() {

	int wD = 290;
	int hD = 420;
#if defined(SYNRAD)
	hD += 150;
#endif

	int buttonW = 90, buttonH = 21;
	int labelH = 13, textH = 20;
	int directionTextW = 110;
	int directionValueX = wD / 2 - directionTextW / 2;
	int	directionLabelX = directionValueX - 50;
	int directionLabelY = 52, directionValueY = directionLabelY - 3;
	int directionCmLabelX = directionValueX + directionTextW + 1;
	int directionSpacingX = 23;

	// Absolute offset, or direction and distance?
	offsetDirectionRadioGroup = std::make_shared<GLRadioGroup>();

	offsetRadioButton = new GLRadioButton(0, "Absolute offset", offsetDirectionRadioGroup);
	offsetRadioButton->SetBounds(6, 6, 96, 17);
	Add(offsetRadioButton);

	directionRadioButton = new GLRadioButton(0, "Direction and distance", offsetDirectionRadioGroup);
	directionRadioButton->SetBounds(140, 6, 125, 17);
	Add(directionRadioButton);

	// Label indicating the choice of abs offset vs dir and dist
	directionLabel = new GLLabel("Absolute offset: ");
	directionLabel->SetBounds(8, 29, 200, labelH);
	Add(directionLabel);

	// X abs offset/dir vector
	dxLabel = new GLLabel("dX:");
	dxLabel->SetBounds(directionLabelX, directionLabelY, 20, labelH);
	Add(dxLabel);

	xText = new GLTextField(0, "0");
	xText->SetBounds(directionValueX, directionValueY, directionTextW, textH);
	Add(xText);

	cmLabelX = new GLLabel("cm");
	cmLabelX->SetBounds(directionCmLabelX, directionLabelY, 21, labelH);
	Add(cmLabelX);

	// Y abs offset/dir vector
	dyLabel = new GLLabel("dY:");
	dyLabel->SetBounds(directionLabelX, directionLabelY + directionSpacingX, 20, labelH);
	Add(dyLabel);

	yText = new GLTextField(0, "0");
	yText->SetBounds(directionValueX, directionValueY + directionSpacingX, directionTextW, textH);
	Add(yText);

	cmLabelY = new GLLabel("cm");
	cmLabelY->SetBounds(directionCmLabelX, directionLabelY + directionSpacingX, 21, labelH);
	Add(cmLabelY);

	// Z abs offset/dir vector
	dzLabel = new GLLabel("dZ:");
	dzLabel->SetBounds(directionLabelX, directionLabelY + 2 * directionSpacingX, 20, labelH);
	Add(dzLabel);

	zText = new GLTextField(0, "0");
	zText->SetBounds(directionValueX, directionValueY + 2 * directionSpacingX, directionTextW, textH);
	Add(zText);

	cmLabelZ = new GLLabel("cm");
	cmLabelZ->SetBounds(directionCmLabelX, directionLabelY + 2 * directionSpacingX, 21, labelH);
	Add(cmLabelZ);

	// Distance (relevant for dir and dist option)
	distanceLabel = new GLLabel("Distance: ");
	distanceLabel->SetBounds(directionValueX - 50, directionLabelY + 3 * directionSpacingX + 4, 50, labelH);
	Add(distanceLabel);

	distanceText = new GLTextField(0, "");
	distanceText->SetBounds(directionValueX, directionValueY + 3 * directionSpacingX + 4, directionTextW, textH);
	Add(distanceText);

	cmLabelDistance = new GLLabel("cm");
	cmLabelDistance->SetBounds(directionCmLabelX, directionLabelY + 3 * directionSpacingX + 4, 21, labelH);
	Add(cmLabelDistance);


	int outerPanelX = 4, outerPanelY = directionLabelY + 4 * directionSpacingX + 5;
	int outerPanelW = wD - 2 * outerPanelX, twoPointPanelH = 115;
	int innerComponentX = 6;
	int innerPanelW = (outerPanelW - 3 * innerComponentX) / 2;

	// Select base facet/vertex and a direction facet/vertex
	// The main panel
	twoPointsPanel = new GLTitledPanel("From two points");
	twoPointsPanel->SetBounds(outerPanelX, outerPanelY,
		outerPanelW, twoPointPanelH);
	Add(twoPointsPanel);

	// A panel for choosing the base
	basePanel = new GLTitledPanel("Base point");
	twoPointsPanel->SetCompBounds(basePanel, innerComponentX, 20, innerPanelW, 90);
	twoPointsPanel->Add(basePanel);

	// A panel for choosing the direction
	dirPanel = new GLTitledPanel("Direction point");
	twoPointsPanel->SetCompBounds(dirPanel, innerComponentX * 2 + innerPanelW, 20, innerPanelW, 90);
	twoPointsPanel->Add(dirPanel);

	// A button to chose base vertex 
	baseVertexButton = new GLButton(0, "Selected vertex");
	basePanel->SetCompBounds(baseVertexButton, innerPanelW / 2 - buttonW / 2, 31, buttonW, buttonH);
	basePanel->Add(baseVertexButton);

	// A button to chose base facet center
	baseFacetCenterButton = new GLButton(0, "Facet center");
	basePanel->SetCompBounds(baseFacetCenterButton, innerPanelW / 2 - buttonW / 2, 60, buttonW, buttonH);
	basePanel->Add(baseFacetCenterButton);

	// A label indicating the chosen base 
	baseStatusLabel = new GLLabel("");
	basePanel->SetCompBounds(baseStatusLabel, 6, 14, 88, labelH);
	basePanel->Add(baseStatusLabel);

	// A button to choose direction vertex 
	dirVertexButton = new GLButton(0, "Selected vertex");
	dirPanel->SetCompBounds(dirVertexButton, innerPanelW / 2 - buttonW / 2, 31, buttonW, buttonH);
	dirPanel->Add(dirVertexButton);

	// A button to choose direction facet center
	dirFacetCenterButton = new GLButton(0, "Facet center");
	dirPanel->SetCompBounds(dirFacetCenterButton, innerPanelW / 2 - buttonW / 2, 60, buttonW, buttonH);
	dirPanel->Add(dirFacetCenterButton);

	// A label indicating the chosen direction
	directionStatusLabel = new GLLabel("Choose base first");
	dirPanel->SetCompBounds(directionStatusLabel, 6, 14, 88, labelH);
	dirPanel->Add(directionStatusLabel);


	int smallButtonW = buttonW - 10;
	int smallButtonSpacingX = wD / 2 - smallButtonW / 2 - innerComponentX - outerPanelX;

	// Define direction using facet properties
	// The main panel
	facetPanel = new GLTitledPanel("From facet");
	facetPanel->SetBounds(outerPanelX, outerPanelY + twoPointPanelH + 5,
		outerPanelW, 95);
	Add(facetPanel);

	// Labels indicating the selected facet and its property
	facetSelectedLabel = new GLLabel("Selected: ");
	facetPanel->SetCompBounds(facetSelectedLabel, innerComponentX, 20, 50, labelH);
	facetPanel->Add(facetSelectedLabel);

	facetSelectedValueLabel = new GLLabel("");
	facetPanel->SetCompBounds(facetSelectedValueLabel, 55, 20, 73, labelH);
	facetPanel->Add(facetSelectedValueLabel);

	// From facet normal
	facetNormalButton = new GLButton(0, "Facet normal");
	facetPanel->SetCompBounds(facetNormalButton, innerComponentX, 40, smallButtonW, buttonH);
	facetPanel->Add(facetNormalButton);

	// From facet U
	facetUButton = new GLButton(0, "Facet U");
	facetPanel->SetCompBounds(facetUButton, innerComponentX + smallButtonSpacingX, 40, smallButtonW, buttonH);
	facetPanel->Add(facetUButton);

	// From facet V
	facetVButton = new GLButton(0, "Facet V");
	facetPanel->SetCompBounds(facetVButton, innerComponentX + 2 * smallButtonSpacingX, 40, smallButtonW, buttonH);
	facetPanel->Add(facetVButton);

	// Facet N x X
	facetNXButton = new GLButton(0, "Facet N x X");
	facetPanel->SetCompBounds(facetNXButton, innerComponentX, 66, smallButtonW, buttonH);
	facetPanel->Add(facetNXButton);

	// Facet N x Y
	facetNYButton = new GLButton(0, "Facet N x Y");
	facetPanel->SetCompBounds(facetNYButton, innerComponentX + smallButtonSpacingX, 66, smallButtonW, buttonH);
	facetPanel->Add(facetNYButton);

	// Facet N x Z
	facetNZButton = new GLButton(0, "Facet N x Z");
	facetPanel->SetCompBounds(facetNZButton, innerComponentX + 2 * smallButtonSpacingX, 66, smallButtonW, buttonH);
	facetPanel->Add(facetNZButton);

#if defined(SYNRAD)
	// Define direction using trajectoryPoints
	// The main panel
	trajPanel = new GLTitledPanel("From trajectory points");
	trajPanel->SetBounds(outerPanelX, outerPanelY + twoPointPanelH + 105,
		outerPanelW, twoPointPanelH + 30);
	Add(trajPanel);

	// A panel for choosing the base
	baseTrajPanel = new GLTitledPanel("Base trajectory point");
	trajPanel->SetCompBounds(baseTrajPanel, innerComponentX, 20, innerPanelW, 60);
	trajPanel->Add(baseTrajPanel);

	// A panel for choosing the direction
	dirTrajPanel = new GLTitledPanel("Direction point");
	trajPanel->SetCompBounds(dirTrajPanel, innerComponentX * 2 + innerPanelW, 20, innerPanelW, 60);
	trajPanel->Add(dirTrajPanel);

	// A button to chose base trajectory point 
	baseTrajPointButton = new GLButton(0, "Selected point");
	baseTrajPanel->SetCompBounds(baseTrajPointButton, innerPanelW / 2 - buttonW / 2, 31, buttonW, buttonH);
	baseTrajPanel->Add(baseTrajPointButton);

	// A button to chose direction trajectory point 
	dirTrajPointButton = new GLButton(0, "Selected point");
	dirTrajPanel->SetCompBounds(dirTrajPointButton, innerPanelW / 2 - buttonW / 2, 31, buttonW, buttonH);
	dirTrajPanel->Add(dirTrajPointButton);
	dirTrajPointButton->SetEnabled(false);

	// A label indicating the chosen base 
	trajBaseStatusLabel = new GLLabel("base status");
	baseTrajPanel->SetCompBounds(trajBaseStatusLabel, 6, 14, 88, labelH);
	trajBaseStatusLabel->SetText("");
	baseTrajPanel->Add(trajBaseStatusLabel);

	// A label indicating the chosen direction
	trajDirectionStatusLabel = new GLLabel("Choose base first");
	dirTrajPanel->SetCompBounds(trajDirectionStatusLabel, 6, 14, 88, labelH);
	dirTrajPanel->Add(trajDirectionStatusLabel);

	// Include rotation?
	trajRotationCheckbox = new GLToggle(0, "Include rotation");
	trajPanel->SetCompBounds(trajRotationCheckbox, innerComponentX, 85, 96, 17);
	trajRotationCheckbox->SetEnabled(false);
	Add(trajRotationCheckbox);

	// A label indicating the rotation axis
	rotAxisLabel = new GLLabel("Rotation axis: ");
	trajPanel->SetCompBounds(rotAxisLabel, innerComponentX, 103, 96, 17);
	Add(rotAxisLabel);

	// A label indicating the rotation angle
	rotAngleLabel = new GLLabel("Rotation angle: ");
	trajPanel->SetCompBounds(rotAngleLabel, innerComponentX, 121, 96, 17);
	Add(rotAngleLabel);
#endif

	int moveButtonX = wD / 4 - buttonW / 2;
	int copyButtonX = moveButtonX + wD / 2;

	moveButton = new GLButton(0, "Move facets");
	moveButton->SetBounds(moveButtonX, hD - 50, buttonW, buttonH);
	Add(moveButton);

	copyButton = new GLButton(0, "Copy facets");
	copyButton->SetBounds(copyButtonX, hD - 50, buttonW, buttonH);
	Add(copyButton);

	SetTitle("Move facet");
	// Center dialog
	int wS, hS;
	GLToolkit::GetWindowSize(&wS, &hS);
	int xD = (wS - wD) / 2;
	int yD = (hS - hD) / 2;
	SetBounds(xD, yD, wD, hD);

	RestoreDeviceObjects();

	// Set absolute offset mode
	offsetDirectionRadioGroup->Select(offsetRadioButton);
	SwitchToOffsetMode();

	interfGeom = g;
	work = w;
	dirVertexButton->SetEnabled(false);
	dirFacetCenterButton->SetEnabled(false);

}

void MoveFacet::ProcessMessage(GLComponent* src, int message) {
	//Initialize values as they will be passed on as parameters to the MoveFacet() function, regardless of delta or distance mode
	double dX = 0.0;
	double dY = 0.0;
	double dZ = 0.0;
	double distance = 0.0;

	switch(message) {
	case MSG_BUTTON:

		if (src==moveButton || src==copyButton) {
			if (interfGeom->GetNbSelectedFacets()==0) {
				GLMessageBox::Display("No facets selected","Nothing to move",GLDLG_OK,GLDLG_ICONERROR);
				return;
			}
			if( !xText->GetNumber(&dX) ) {
				GLMessageBox::Display("Invalid X offset/direction","Error",GLDLG_OK,GLDLG_ICONERROR);
				return;
			}
			if( !yText->GetNumber(&dY) ) {
				GLMessageBox::Display("Invalid Y offset/direction","Error",GLDLG_OK,GLDLG_ICONERROR);
				return;
			}
			if( !zText->GetNumber(&dZ) ) {
				GLMessageBox::Display("Invalid Z offset/direction","Error",GLDLG_OK,GLDLG_ICONERROR);
				return;
			}
			bool towardsDirectionMode = directionRadioButton->GetSelected();
			if ( towardsDirectionMode && !distanceText->GetNumber(&distance) ) {
				GLMessageBox::Display("Invalid offset distance","Error",GLDLG_OK,GLDLG_ICONERROR);
				return;
			}
			if (towardsDirectionMode && dX==0.0 && dY==0.0 && dZ==0.0) {
				GLMessageBox::Display("Direction can't be null-vector", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			if (mApp->AskToReset()){

				interfGeom->MoveSelectedFacets(dX,dY,dZ,towardsDirectionMode, distance, src==copyButton);
#if defined(SYNRAD)
				if (trajRotationCheckbox->GetState() == 1) {
					// Never set copy=true here, the copy is already created by MoveSelectedFacets
					geom->RotateSelectedFacets(rotAxisP0, rotAxisDir, rotAngle, false, work);
				}
#endif
				work->MarkToReload(); 
				mApp->changedSinceSave = true;
				mApp->UpdateFacetlistSelected();	
				mApp->UpdateViewers();
			}
		}
		else if (src == facetNormalButton) {
			auto selFacets = interfGeom->GetSelectedFacets();
			if (selFacets.size() != 1) {
				GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			std::string tmp = fmt::format("Facet {} normal", selFacets[0] + 1);
			facetSelectedValueLabel->SetText(tmp);

			Vector3d facetNormal = interfGeom->GetFacet(selFacets[0])->sh.N;
			xText->SetText(facetNormal.x);
			yText->SetText(facetNormal.y);
			zText->SetText(facetNormal.z);

			//Switch to direction mode
			offsetDirectionRadioGroup->Select(directionRadioButton);
			SwitchToDirectionMode();
		}
		else if (src == baseVertexButton) {
			auto selVertices = interfGeom->GetSelectedVertices();
			if (selVertices.size() != 1) {
				GLMessageBox::Display("Select exactly one vertex", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}
			baseLocation = (Vector3d)*(interfGeom->GetVertex(selVertices[0]));
			baseLocationSet = true;
			std::string tmp = fmt::format("Vertex {}", selVertices[0] + 1);
			baseStatusLabel->SetText(tmp);
			baseStatusLabel->SetText(fmt::format("Vertex {}", selVertices[0] + 1));
			dirFacetCenterButton->SetEnabled(true);
			dirVertexButton->SetEnabled(true);
		}
		else if (src == baseFacetCenterButton) {
			auto selFacets = interfGeom->GetSelectedFacets();
			if (selFacets.size() != 1) {
				GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}
			baseLocation = interfGeom->GetFacet(selFacets[0])->sh.center;
			baseLocationSet = true;
			std::string tmp = fmt::format("Center of facet {}", selFacets[0] + 1);
			baseStatusLabel->SetText(tmp);
			dirFacetCenterButton->SetEnabled(true);
			dirVertexButton->SetEnabled(true);
		}
		else if (src == dirVertexButton) { //only enabled once base is defined
			auto selVertices = interfGeom->GetSelectedVertices();
			if (selVertices.size() != 1) {
				GLMessageBox::Display("Select exactly one vertex", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}
			Vector3d translation = *(interfGeom->GetVertex(selVertices[0])) - baseLocation;

			xText->SetText(translation.x);
			yText->SetText(translation.y);
			zText->SetText(translation.z);
			distanceText->SetText(translation.Norme());

			std::string tmp = fmt::format("Vertex {}", selVertices[0] + 1);
			directionStatusLabel->SetText(tmp);
		}
		else if (src == dirFacetCenterButton) { //only enabled once base is defined
			auto selFacets = interfGeom->GetSelectedFacets();
			if (selFacets.size() != 1) {
				GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}
			Vector3d translation = interfGeom->GetFacet(selFacets[0])->sh.center - baseLocation;

			xText->SetText(translation.x);
			yText->SetText(translation.y);
			zText->SetText(translation.z);
			distanceText->SetText(translation.Norme());

			std::string tmp = fmt::format("Center of facet {}", selFacets[0] + 1);
			directionStatusLabel->SetText(tmp);
		}
		else if (src == facetUButton) {
			auto selFacets = interfGeom->GetSelectedFacets();
			if (selFacets.size() != 1) {
				GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			std::string tmp = fmt::format("Facet {} \201", selFacets[0] + 1);
			facetSelectedValueLabel->SetText(tmp);
			xText->SetText(interfGeom->GetFacet(selFacets[0])->sh.U.x);
			yText->SetText(interfGeom->GetFacet(selFacets[0])->sh.U.y);
			zText->SetText(interfGeom->GetFacet(selFacets[0])->sh.U.z);
			distanceText->SetText(interfGeom->GetFacet(selFacets[0])->sh.U.Norme());
		}
		else if (src == facetVButton) {
			auto selFacets = interfGeom->GetSelectedFacets();
			if (selFacets.size() != 1) {
				GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			std::string tmp = fmt::format("Facet {} \202", selFacets[0] + 1);
			facetSelectedValueLabel->SetText(tmp);
			xText->SetText(interfGeom->GetFacet(selFacets[0])->sh.V.x);
			yText->SetText(interfGeom->GetFacet(selFacets[0])->sh.V.y);
			zText->SetText(interfGeom->GetFacet(selFacets[0])->sh.V.z);
			distanceText->SetText(interfGeom->GetFacet(selFacets[0])->sh.V.Norme());
		}
		else if (src == facetNXButton) {
			auto selFacets = interfGeom->GetSelectedFacets();
			if (selFacets.size() != 1) {
				GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			std::string tmp = fmt::format("Facet {} N x X", selFacets[0] + 1);
			facetSelectedValueLabel->SetText(tmp);
			xText->SetText(0);
			yText->SetText(interfGeom->GetFacet(selFacets[0])->sh.N.z);
			zText->SetText(-interfGeom->GetFacet(selFacets[0])->sh.N.y);

			//Switch to direction mode
			offsetDirectionRadioGroup->Select(directionRadioButton);
			SwitchToDirectionMode();
		}
		else if (src == facetNYButton) {
			auto selFacets = interfGeom->GetSelectedFacets();
			if (selFacets.size() != 1) {
				GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			std::string tmp = fmt::format("Facet {} N x Y", selFacets[0] + 1);
			facetSelectedValueLabel->SetText(tmp);
			xText->SetText(-interfGeom->GetFacet(selFacets[0])->sh.N.z);
			yText->SetText(0);
			zText->SetText(interfGeom->GetFacet(selFacets[0])->sh.N.x);

			//Switch to direction mode
			offsetDirectionRadioGroup->Select(directionRadioButton);
			SwitchToDirectionMode();
		}
		else if (src == facetNZButton) {
			auto selFacets = interfGeom->GetSelectedFacets();
			if (selFacets.size() != 1) {
				GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			std::string tmp = fmt::format("Facet {} N x Z", selFacets[0] + 1);
			facetSelectedValueLabel->SetText(tmp);
			xText->SetText(interfGeom->GetFacet(selFacets[0])->sh.N.y);
			yText->SetText(-interfGeom->GetFacet(selFacets[0])->sh.N.x);
			zText->SetText(0);

			//Switch to direction mode
			offsetDirectionRadioGroup->Select(directionRadioButton);
			SwitchToDirectionMode();
		}
#if defined(SYNRAD)
		else if (src == baseTrajPointButton) {
			std::vector<size_t> selRegions = GetSelectedRegions();
			if (selRegions.size() != 1) {
				GLMessageBox::Display("Select exactly one trajectory point", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}

			const Region_full& region = work->regions[selRegions[0]];
			int pointId = region.selectedPointId;
			baseTrajPoint = region.Points[pointId];

			std::string tmp = fmt::format("Region {}, point {}", selRegions[0] + 1, pointId + 1);
			trajBaseStatusLabel->SetText(tmp);
			dirTrajPointButton->SetEnabled(true);
			}
		else if (src == dirTrajPointButton) {
				std::vector<size_t> selRegions = GetSelectedRegions();
				if (selRegions.size() != 1) {
					GLMessageBox::Display("Select exactly one trajectory point", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}

				const Region_full& region = work->regions[selRegions[0]];
				int pointId = region.selectedPointId;
				const Trajectory_Point& dirPoint = region.Points[pointId];
				Vector3d translation = dirPoint.position - baseTrajPoint.position;
				if (dirPoint.direction == baseTrajPoint.direction) {
					rotAxisDir = Vector3d(0, 0, 0);
					rotAngle = 0;
				}
				else if (dirPoint.direction == -1.0 * baseTrajPoint.direction) {
					// Rotate by 180 deg if vectors are absolute opposites
					// Use a new vector to calculate (any) rotation axis perpendicular to base and dir
					Vector3d arbitraryVector(0.0, 1.0, 0.0);
					if (!IsEqual(std::abs(dirPoint.direction.y), 0.0)) { //edge case where direction points +/- Y
						arbitraryVector = Vector3d(1.0, 0.0, 0.0);
					}

					// Cross product produces an axis perpendicular to base vector -> also perpendicular to dir vector
					// Angle is always PI rad (180 deg) between opposite vectors
					rotAxisDir = CrossProduct(baseTrajPoint.direction, arbitraryVector).Normalized();
					rotAngle = PI;
				}
				else {
					rotAxisDir = CrossProduct(baseTrajPoint.direction, dirPoint.direction).Normalized();
					rotAngle = acos(Dot(baseTrajPoint.direction, dirPoint.direction));
				}
				rotAxisP0 = dirPoint.position;
				trajRotationCheckbox->SetEnabled(true);

				xText->SetText(translation.x);
				yText->SetText(translation.y);
				zText->SetText(translation.z);
				distanceText->SetText(translation.Norme());

				rotAxisLabel->SetText(fmt::format("Rotation axis: {:g}  {:g}  {:g}",
					rotAxisDir.x, rotAxisDir.y, rotAxisDir.z));
				rotAngleLabel->SetText(fmt::format("Rotation angle: {:g} rad", rotAngle));
				std::string tmp = fmt::format("Region {}, point {}", selRegions[0] + 1, pointId + 1);
				trajDirectionStatusLabel->SetText(tmp);
				}
#endif
		break;
#if defined(SYNRAD)
	case MSG_TOGGLE:
		if (src == trajRotationCheckbox)
		{
			if (trajRotationCheckbox->GetState() == 1) {
				baseFacetCenterButton->SetEnabled(false);
				baseVertexButton->SetEnabled(false);
				dirFacetCenterButton->SetEnabled(false);
				dirVertexButton->SetEnabled(false);
				facetNormalButton->SetEnabled(false);
				facetUButton->SetEnabled(false);
				facetVButton->SetEnabled(false);
				facetNXButton->SetEnabled(false);
				facetNYButton->SetEnabled(false);
				facetNZButton->SetEnabled(false);
			}
			else {
				baseFacetCenterButton->SetEnabled(true);
				baseVertexButton->SetEnabled(true);
				dirFacetCenterButton->SetEnabled(baseLocationSet);
				dirVertexButton->SetEnabled(baseLocationSet);
				facetNormalButton->SetEnabled(true);
				facetUButton->SetEnabled(true);
				facetVButton->SetEnabled(true);
				facetNXButton->SetEnabled(true);
				facetNYButton->SetEnabled(true);
				facetNZButton->SetEnabled(true);
			}
		}
#endif
	case MSG_RADIO:
		bool towardsDirectionMode = src == directionRadioButton;
		if (towardsDirectionMode)
			SwitchToDirectionMode();
		else
			SwitchToOffsetMode();
	}

	GLWindow::ProcessMessage(src,message);
}

void MoveFacet::SwitchToDirectionMode() {
	distanceText->SetEditable(true);
	directionLabel->SetText("Direction vector and distance: ");
	dxLabel->SetText("dir. X:");
	dyLabel->SetText("dir. Y:");
	dzLabel->SetText("dir. Z:");
}

void MoveFacet::SwitchToOffsetMode() {
	distanceText->SetEditable(false);
	directionLabel->SetText("Absolute offset: ");
	dxLabel->SetText("dX:");
	dyLabel->SetText("dY:");
	dzLabel->SetText("dZ:");
}


#if defined(SYNRAD)
// TODO: Move to another class, e.g. InterfaceGeometry
std::vector<size_t> MoveFacet::GetSelectedRegions() {
	std::vector<size_t> result;
	result.reserve(work->regions.size());
	for (size_t i = 0; i < work->regions.size(); i++) {
		if (work->regions[i].selectedPointId > -1) {
			result.push_back(i);
		}
	}
	return result;
}
#endif

