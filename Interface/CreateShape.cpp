
#include "CreateShape.h"
#include "GLAppGui/GLTitledPanel.h"
#include "GLAppGui/GLToolkit.h"
#include "GLAppGui/GLWindowManager.h"
#include "GLAppGui/GLMessageBox.h"
#include "GLAppGui/GLTextField.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLRadioGroup.h"
#include "GLAppGui/GLRadioButton.h"
#include "Geometry_shared.h"
#include "Facet_shared.h"
#include "Helper/MathTools.h" //Contains
#include "GLAppCore/GLMathTools.h"
#include "GLAppGui/GLIcon.h"

#if defined(MOLFLOW)
#include "../../src/MolFlow.h"
#endif

#if defined(SYNRAD)
#include "../src/SynRad.h"
#endif

#if defined(MOLFLOW)
extern MolFlow *mApp;
#endif

#if defined(SYNRAD)
extern SynRad*mApp;
#endif

/**
* \brief Constructor with initialisation for the CreateShape window (Facet/Create shape)
* \param g pointer to the InterfaceGeometry
* \param w Worker handle
*/
CreateShape::CreateShape(InterfaceGeometry *g,Worker *w):GLWindow() {

	int wD = 713;
	int hD = 478;
	shapePanel = new GLTitledPanel("Shape");
	shapePanel->SetBounds(8, 3, 694, 247);
	Add(shapePanel);
	positionPanel = new GLTitledPanel("Position");
	positionPanel->SetBounds(8, 256, 694, 94);
	Add(positionPanel);
	sizePanel = new GLTitledPanel("Size");
	sizePanel->SetBounds(8, 356, 694, 66);
	Add(sizePanel);

	shapeRadioGroup = std::make_shared<GLRadioGroup>();

	racetrackRadioButton = new GLRadioButton(0, "Racetrack", shapeRadioGroup);
	shapePanel->SetCompBounds(racetrackRadioButton, 424, 19, 76, 17);
	shapePanel->Add(racetrackRadioButton);

	ellipseRadioButton = new GLRadioButton(0, "Circle / Ellipse", shapeRadioGroup);
	shapePanel->SetCompBounds(ellipseRadioButton, 308, 19, 93, 17);
	shapePanel->Add(ellipseRadioButton);

	rectangleRadioButton = new GLRadioButton(0, "Square / Rectangle", shapeRadioGroup);
	shapePanel->SetCompBounds(rectangleRadioButton, 182, 19, 120, 17);
	shapePanel->Add(rectangleRadioButton);

	normalStatusLabel = new GLLabel("status");
	positionPanel->SetCompBounds(normalStatusLabel, 594, 68, 35, 13);
	positionPanel->Add(normalStatusLabel);

	normalVertexButton = new GLButton(0, "Center to vertex");
	positionPanel->SetCompBounds(normalVertexButton, 506, 65, 85, 20);
	positionPanel->Add(normalVertexButton);

	facetNormalButton = new GLButton(0, "Facet N");
	positionPanel->SetCompBounds(facetNormalButton, 425, 65, 75, 20);
	positionPanel->Add(facetNormalButton);

	axisStatusLabel = new GLLabel("status");
	positionPanel->SetCompBounds(axisStatusLabel, 594, 42, 35, 13);
	positionPanel->Add(axisStatusLabel);

	axisVertexButton = new GLButton(0, "Center to vertex");
	positionPanel->SetCompBounds(axisVertexButton, 506, 39, 85, 20);
	positionPanel->Add(axisVertexButton);

	axisFacetUButton = new GLButton(0, "Facet U");
	positionPanel->SetCompBounds(axisFacetUButton, 425, 39, 75, 20);
	positionPanel->Add(axisFacetUButton);

	centerStatusLabel = new GLLabel("status");
	positionPanel->SetCompBounds(centerStatusLabel, 594, 16, 35, 13);
	positionPanel->Add(centerStatusLabel);

	centerVertexButton = new GLButton(0, "Vertex");
	positionPanel->SetCompBounds(centerVertexButton, 506, 13, 85, 20);
	positionPanel->Add(centerVertexButton);

	facetCenterButton = new GLButton(0, "Facet center");
	positionPanel->SetCompBounds(facetCenterButton, 425, 13, 75, 20);
	positionPanel->Add(facetCenterButton);

	normalZtext = new GLTextField(0, "1");
	positionPanel->SetCompBounds(normalZtext, 339, 65, 80, 20);
	positionPanel->Add(normalZtext);

	normalZLabel = new GLLabel("Z:");
	positionPanel->SetCompBounds(normalZLabel, 322, 68, 17, 13);
	positionPanel->Add(normalZLabel);

	normalYtext = new GLTextField(0, "0");
	positionPanel->SetCompBounds(normalYtext, 232, 65, 80, 20);
	positionPanel->Add(normalYtext);

	normalYLabel = new GLLabel("Y:");
	positionPanel->SetCompBounds(normalYLabel, 215, 68, 17, 13);
	positionPanel->Add(normalYLabel);

	normalXtext = new GLTextField(0, "0");
	positionPanel->SetCompBounds(normalXtext, 128, 65, 80, 20);
	positionPanel->Add(normalXtext);

	normalXLabel = new GLLabel("X:");
	positionPanel->SetCompBounds(normalXLabel, 111, 68, 17, 13);
	positionPanel->Add(normalXLabel);

	normalLabel = new GLLabel("Normal direction:");
	positionPanel->SetCompBounds(normalLabel, 6, 68, 86, 13);
	positionPanel->Add(normalLabel);

	axisDirZtext = new GLTextField(0, "0");
	positionPanel->SetCompBounds(axisDirZtext, 339, 40, 80, 20);
	positionPanel->Add(axisDirZtext);

	axisDirZLabel = new GLLabel("Z:");
	positionPanel->SetCompBounds(axisDirZLabel, 322, 42, 17, 13);
	positionPanel->Add(axisDirZLabel);

	axisDirYtext = new GLTextField(0, "0");
	positionPanel->SetCompBounds(axisDirYtext, 232, 39, 80, 20);
	positionPanel->Add(axisDirYtext);

	axisDirYLabel = new GLLabel("Y:");
	positionPanel->SetCompBounds(axisDirYLabel, 215, 42, 17, 13);
	positionPanel->Add(axisDirYLabel);

	axisDirXtext = new GLTextField(0, "1");
	positionPanel->SetCompBounds(axisDirXtext, 128, 39, 80, 20);
	positionPanel->Add(axisDirXtext);

	axisDirXLabel = new GLLabel("X:");
	positionPanel->SetCompBounds(axisDirXLabel, 111, 42, 17, 13);
	positionPanel->Add(axisDirXLabel);

	axisDirLabel = new GLLabel("Axis1 direction:");
	positionPanel->SetCompBounds(axisDirLabel, 6, 42, 78, 13);
	positionPanel->Add(axisDirLabel);

	centerZtext = new GLTextField(0, "0");
	positionPanel->SetCompBounds(centerZtext, 339, 13, 80, 20);
	positionPanel->Add(centerZtext);

	centerZLabel = new GLLabel("Z:");
	positionPanel->SetCompBounds(centerZLabel, 322, 16, 17, 13);
	positionPanel->Add(centerZLabel);

	centerYtext = new GLTextField(0, "0");
	positionPanel->SetCompBounds(centerYtext, 232, 13, 80, 20);
	positionPanel->Add(centerYtext);

	centerYLabel = new GLLabel("Y:");
	positionPanel->SetCompBounds(centerYLabel, 215, 16, 17, 13);
	positionPanel->Add(centerYLabel);

	centerXtext = new GLTextField(0, "0");
	positionPanel->SetCompBounds(centerXtext, 128, 13, 80, 20);
	positionPanel->Add(centerXtext);

	centerXLabel = new GLLabel("X:");
	positionPanel->SetCompBounds(centerXLabel, 111, 16, 17, 13);
	positionPanel->Add(centerXLabel);

	centerLabel = new GLLabel("Center:");
	positionPanel->SetCompBounds(centerLabel, 6, 16, 41, 13);
	positionPanel->Add(centerLabel);

	fullCircleButton = new GLButton(0, "Full circle sides");
	sizePanel->SetCompBounds(fullCircleButton, 449, 13, 142, 20);
	sizePanel->Add(fullCircleButton);

	nbStepsText = new GLTextField(0, "10");
	sizePanel->SetCompBounds(nbStepsText, 339, 39, 80, 20);
	sizePanel->Add(nbStepsText);

	nbStepsLabel = new GLLabel("Steps in arc:");
	sizePanel->SetCompBounds(nbStepsLabel, 229, 42, 66, 13);
	sizePanel->Add(nbStepsLabel);

	racetrackTopCmLabel = new GLLabel("cm");
	sizePanel->SetCompBounds(racetrackTopCmLabel, 422, 16, 21, 13);
	sizePanel->Add(racetrackTopCmLabel);

	racetrackTopLengthText = new GLTextField(0, "");
	sizePanel->SetCompBounds(racetrackTopLengthText, 339, 13, 80, 20);
	sizePanel->Add(racetrackTopLengthText);

	racetrackTopLengthLabel = new GLLabel("Racetrack top length:");
	sizePanel->SetCompBounds(racetrackTopLengthLabel, 229, 16, 110, 13);
	sizePanel->Add(racetrackTopLengthLabel);

	aix2CmLabel = new GLLabel("cm");
	sizePanel->SetCompBounds(aix2CmLabel, 161, 42, 21, 13);
	sizePanel->Add(aix2CmLabel);

	axis2LengthText = new GLTextField(0, "1");
	sizePanel->SetCompBounds(axis2LengthText, 79, 39, 80, 20);
	sizePanel->Add(axis2LengthText);

	axis2LengthLabel = new GLLabel("Axis2 length:");
	sizePanel->SetCompBounds(axis2LengthLabel, 6, 42, 67, 13);
	sizePanel->Add(axis2LengthLabel);

	axis1CmLabel = new GLLabel("cm");
	sizePanel->SetCompBounds(axis1CmLabel, 161, 16, 21, 13);
	sizePanel->Add(axis1CmLabel);

	axis1LengthText = new GLTextField(0, "1");
	sizePanel->SetCompBounds(axis1LengthText, 79, 13, 80, 20);
	sizePanel->Add(axis1LengthText);

	axis1LengthLabel = new GLLabel("Axis1 length:");
	sizePanel->SetCompBounds(axis1LengthLabel, 6, 16, 67, 13);
	sizePanel->Add(axis1LengthLabel);

	createButton = new GLButton(0, "Create facet");
	createButton->SetBounds(327, 431, 100, 20);
	Add(createButton);



	//------------------------------------------------------

	rectangleIcon = new GLIcon("images/edit_rectangle.png");
	rectangleIcon->SetBounds(80, 43, 500, 190);
	rectangleIcon->SetVisible(true);
	Add(rectangleIcon);
	circleIcon = new GLIcon("images/edit_circle.png");
	circleIcon->SetBounds(80, 43, 500, 190);
	circleIcon->SetVisible(false);
	Add(circleIcon);
	racetrackIcon = new GLIcon("images/edit_racetrack.png");
	racetrackIcon->SetBounds(80, 43, 500, 190);
	racetrackIcon->SetVisible(false);
	Add(racetrackIcon);

	SetTitle("Create shape");
	// Center dialog
	//int wS,hS;
	//GLToolkit::GetWindowSize(&wS,&hS);
	int xD = 7; //quite wide window for centering!
	int yD = 29;
	SetBounds(xD,yD,wD,hD);


  RestoreDeviceObjects();

  interfGeom = g;
  work = w;
  mode = MODE_RECTANGLE;
  shapeRadioGroup->Select(rectangleRadioButton);
  EnableDisableControls();
  centerStatusLabel->SetText("");
  axisStatusLabel->SetText("");
  normalStatusLabel->SetText("");
}

/**
* \brief Function for processing various inputs (button, check boxes etc.)
* \param src Exact source of the call
* \param message Type of the source (button)
*/
void CreateShape::ProcessMessage(GLComponent *src,int message) {

  switch (message) {
  case MSG_BUTTON:
	  if (src == createButton) {
          if (mApp->AskToReset()) {
		  Vector3d center, axisDir, normalDir;
		  double axis1length, axis2length, racetrackTopLength;
		  int nbSteps;

		  if (!centerXtext->GetNumber(&center.x)) {
			  GLMessageBox::Display("Invalid center X coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!centerYtext->GetNumber(&center.y)) {
			  GLMessageBox::Display("Invalid center Y coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!centerZtext->GetNumber(&center.z)) {
			  GLMessageBox::Display("Invalid center Z coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }

		  if (!axisDirXtext->GetNumber(&axisDir.x)) {
			  GLMessageBox::Display("Invalid Axis1 direction X coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!axisDirYtext->GetNumber(&axisDir.y)) {
			  GLMessageBox::Display("Invalid Axis1 direction Y coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!axisDirZtext->GetNumber(&axisDir.z)) {
			  GLMessageBox::Display("Invalid Axis1 direction Z coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }

		  if (!normalXtext->GetNumber(&normalDir.x)) {
			  GLMessageBox::Display("Invalid normal direction X coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!normalYtext->GetNumber(&normalDir.y)) {
			  GLMessageBox::Display("Invalid normal direction Y coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!normalZtext->GetNumber(&normalDir.z)) {
			  GLMessageBox::Display("Invalid normal direction Z coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }

		  if (IsEqual(axisDir.Norme(), 0.0)) {
			  GLMessageBox::Display("Axis1 direction can't be null-vector", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (IsEqual(normalDir.Norme(),0.0)) {
			  GLMessageBox::Display("Normal direction can't be null-vector", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }

		  if (!axis1LengthText->GetNumber(&axis1length) || !(axis1length>0.0)) {
			  GLMessageBox::Display("Invalid axis1 length", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }

		  if (!axis2LengthText->GetNumber(&axis2length) || !(axis2length>0.0)) {
			  GLMessageBox::Display("Invalid axis2 length", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }

		  if (mode == MODE_RACETRACK) {
			  if (!racetrackTopLengthText->GetNumber(&racetrackTopLength)) {
				  GLMessageBox::Display("Can't parse racetrack top length", "Error", GLDLG_OK, GLDLG_ICONERROR);
				  return;
			  }
			  if (racetrackTopLength >= axis1length) {
				  GLMessageBox::Display("For a racetrack, the top length must be less than Axis1", "Error", GLDLG_OK, GLDLG_ICONERROR);
				  return;
			  } 
			  if (!(racetrackTopLength >= (axis1length - axis2length))) {
				  GLMessageBox::Display("For a racetrack, the top length must be at least (Axis1 - Axis2)", "Error", GLDLG_OK, GLDLG_ICONERROR);
				  return;
			  }
			  if (!(racetrackTopLength > 0.0)) {
				  GLMessageBox::Display("Top length must be positive", "Error", GLDLG_OK, GLDLG_ICONERROR);
				  return;
			  }
		  }
		  if (GLApp_MathTools::Contains({ MODE_CIRCLE,MODE_RACETRACK }, mode)) {
			  if (!nbStepsText->GetNumberInt(&nbSteps)) {
				  GLMessageBox::Display("Can't parse number of steps in arc", "Error", GLDLG_OK, GLDLG_ICONERROR);
				  return;
			  }
			  if (!(nbSteps>=2)) {
				  GLMessageBox::Display("Number of arc steps must be at least 2", "Error", GLDLG_OK, GLDLG_ICONERROR);
				  return;
			  }
		  }

		  switch (mode) {
		  case MODE_RECTANGLE:
			  interfGeom->CreateRectangle(center, axisDir, normalDir, axis1length, axis2length);
			  break;
		  case MODE_CIRCLE:
			  interfGeom->CreateCircle(center, axisDir, normalDir, axis1length, axis2length, (size_t)nbSteps);
			  break;
		  case MODE_RACETRACK:
			  interfGeom->CreateRacetrack(center, axisDir, normalDir, axis1length, axis2length, racetrackTopLength, (size_t)nbSteps);
			  break;
		  }
		  work->MarkToReload();
		  mApp->changedSinceSave = true;
		  mApp->UpdateFacetlistSelected();
          }
	  }
	  else if (src == fullCircleButton) {
		  double axis1length, axis2length;
		  if (!axis1LengthText->GetNumber(&axis1length) || !(axis1length>0.0)) {
			  GLMessageBox::Display("Invalid axis1 length", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }

		  if (!axis2LengthText->GetNumber(&axis2length) || !(axis2length>0.0)) {
			  GLMessageBox::Display("Invalid axis2 length", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  racetrackTopLengthText->SetText(axis1length-axis2length);
	  }
	  else if (src == facetCenterButton) {
		  auto selFacets = interfGeom->GetSelectedFacets();
		  if (selFacets.size() != 1) {
			  GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  Vector3d facetCenter = interfGeom->GetFacet(selFacets[0])->sh.center;
		  
		  centerXtext->SetText(facetCenter.x);
		  centerYtext->SetText(facetCenter.y);
		  centerZtext->SetText(facetCenter.z);
		  
		  std::stringstream tmp; tmp << "Center of facet " << selFacets[0] + 1;
		  centerStatusLabel->SetText(tmp.str());
	  }
	  else if (src == centerVertexButton) {
		  auto selVertices = interfGeom->GetSelectedVertices();
		  if (selVertices.size() != 1) {
			  GLMessageBox::Display("Select exactly one vertex", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  Vector3d center = *(interfGeom->GetVertex(selVertices[0]));

		  centerXtext->SetText(center.x);
		  centerYtext->SetText(center.y);
		  centerZtext->SetText(center.z);

		  std::stringstream tmp; tmp << "Vertex " << selVertices[0] + 1;
		  centerStatusLabel->SetText(tmp.str());
	  }
	  else if (src == axisFacetUButton) {
		  auto selFacets = interfGeom->GetSelectedFacets();
		  if (selFacets.size() != 1) {
			  GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  Vector3d facetU = interfGeom->GetFacet(selFacets[0])->sh.U;

		  axisDirXtext->SetText(facetU.x);
		  axisDirYtext->SetText(facetU.y);
		  axisDirZtext->SetText(facetU.z);

		  std::stringstream tmp; tmp << "Facet " << selFacets[0] + 1 << " \201";
		  axisStatusLabel->SetText(tmp.str());
	  }
	  else if (src == axisVertexButton) {
		  Vector3d center;
		  if (!centerXtext->GetNumber(&center.x)) {
			  GLMessageBox::Display("Invalid center X coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!centerYtext->GetNumber(&center.y)) {
			  GLMessageBox::Display("Invalid center Y coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!centerZtext->GetNumber(&center.z)) {
			  GLMessageBox::Display("Invalid center Z coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  
		  auto selVertices = interfGeom->GetSelectedVertices();
		  if (selVertices.size() != 1) {
			  GLMessageBox::Display("Select exactly one vertex", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  Vector3d vertexLocation = *(interfGeom->GetVertex(selVertices[0]));

		  Vector3d diff = vertexLocation - center;

		  axisDirXtext->SetText(diff.x);
		  axisDirYtext->SetText(diff.y);
		  axisDirZtext->SetText(diff.z);

		  std::stringstream tmp; tmp << "Center to vertex " << selVertices[0] + 1;
		  axisStatusLabel->SetText(tmp.str());
	  }
	  else if (src == facetNormalButton) {
		  auto selFacets = interfGeom->GetSelectedFacets();
		  if (selFacets.size() != 1) {
			  GLMessageBox::Display("Select exactly one facet", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  Vector3d facetN = interfGeom->GetFacet(selFacets[0])->sh.N;

		  normalXtext->SetText(facetN.x);
		  normalYtext->SetText(facetN.y);
		  normalZtext->SetText(facetN.z);

		  std::stringstream tmp; tmp << "Facet " << selFacets[0] + 1 << " normal";
		  normalStatusLabel->SetText(tmp.str());
	  }
	  else if (src == normalVertexButton) {
		  Vector3d center;
		  if (!centerXtext->GetNumber(&center.x)) {
			  GLMessageBox::Display("Invalid center X coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!centerYtext->GetNumber(&center.y)) {
			  GLMessageBox::Display("Invalid center Y coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  if (!centerZtext->GetNumber(&center.z)) {
			  GLMessageBox::Display("Invalid center Z coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }

		  auto selVertices = interfGeom->GetSelectedVertices();
		  if (selVertices.size() != 1) {
			  GLMessageBox::Display("Select exactly one vertex", "Error", GLDLG_OK, GLDLG_ICONERROR);
			  return;
		  }
		  Vector3d vertexLocation = *(interfGeom->GetVertex(selVertices[0]));

		  Vector3d diff = vertexLocation - center;

		  normalXtext->SetText(diff.x);
		  normalYtext->SetText(diff.y);
		  normalZtext->SetText(diff.z);

		  std::stringstream tmp; tmp << "Center to vertex " << selVertices[0] + 1;
		  normalStatusLabel->SetText(tmp.str());
	  }
	  break;
  case MSG_RADIO:
	if (src == rectangleRadioButton) mode = MODE_RECTANGLE;
	else if (src == ellipseRadioButton) mode = MODE_CIRCLE;
	else if (src == racetrackRadioButton) mode = MODE_RACETRACK;

	EnableDisableControls();

	break;
  }

  GLWindow::ProcessMessage(src, message);
}

/**
* \brief Toggles visible shapes and active/inactive boxes
*/
void CreateShape::EnableDisableControls() {
	rectangleIcon->SetVisible(mode == MODE_RECTANGLE);
	circleIcon->SetVisible(mode == MODE_CIRCLE);
	racetrackTopLengthText->SetEditable(mode == MODE_RACETRACK);
	racetrackIcon->SetVisible(mode == MODE_RACETRACK);
	fullCircleButton->SetEnabled(mode == MODE_RACETRACK);
	nbStepsText->SetEditable(GLApp_MathTools::Contains({ MODE_CIRCLE, MODE_RACETRACK }, mode));
}