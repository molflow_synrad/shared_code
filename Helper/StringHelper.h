#pragma once
#include <string>
#include <vector>

template <class T>
T stringToNumber(const std::string& s, bool returnDefValOnErr=false);

extern template int stringToNumber<int>(const std::string& s, bool returnDefValOnErr);
extern template size_t stringToNumber<size_t>(const std::string& s, bool returnDefValOnErr);
extern template double stringToNumber<double>(const std::string& s, bool returnDefValOnErr);

void splitList(std::vector<size_t>& outputIds, std::string inputString, size_t upperLimit);
void splitFacetList(std::vector<size_t>& outputFacetIds, std::string inputString, size_t nbFacets);
std::string AbbreviateString(const std::string& input, size_t maxLength);
std::vector<std::string> SplitString(const std::string & input);
std::vector<std::string> SplitString(const std::string & input, char delimiter);

std::string findAndReplace(std::string text, std::string find, std::string replace);
std::string space2underscore(std::string text);
std::string molflowToAscii(std::string text);
std::string molflowToUnicode(std::string text);
bool iContains(const std::vector<std::string>& vec, const std::string& value);
size_t countLines(const std::string& str, bool countEmpty=true);
size_t countLines(const std::stringstream& ss, bool countEmpty=true);
std::string extract_escaped_content(const std::string& input);

namespace Util {
    std::string getTimepointString();

    template<typename T>
    bool getNumber(T* num, std::string str)
    {
        try {
            *num = static_cast<T>(std::stod(str));
            return true;
        }
        catch (std::exception e)
        {
            return false;
        }
    }
}
