#pragma once

#include <cstddef>
#include <string>
namespace Util {
    std::string formatInt(size_t v, const char *unit);

    std::string formatPs(double v, const char *unit);

    std::string formatSize(size_t size);

    std::string formatTime(float t);
    std::string formatTime(double t);

    std::string GetCurrentDateTime();
}
