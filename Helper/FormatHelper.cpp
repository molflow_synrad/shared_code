

#include "FormatHelper.h"
#include <cstdio>
#include <fmt/core.h>
#include <iostream>
#include <chrono>
#include <iomanip>  // for std::put_time
#include <sstream>  // for std::ostringstream
#include <ctime>    // for std::localtime

namespace Util {

// Name: formatInt()
// Desc: Format an integer in K,M,G,..
    std::string formatInt(size_t v, const char *unit) {
        double x = (double)v;

        if (x < 1E3) {
            return fmt::format("{} {}", x, unit);
        } else if (x < 1E6) {
            return fmt::format("{:.1f} K{}", x / 1E3, unit);
        } else if (x < 1E9) {
            return fmt::format("{:.2f} M{}", x / 1E6, unit);
        } else if (x < 1E12) {
            return fmt::format("{:.2f} G{}", x / 1E9, unit);
        } else {
            return fmt::format("{:.2f} T{}", x / 1E12, unit);
        }
    }

// Name: formatPs()
// Desc: Format a double in K,M,G,.. per sec
    std::string formatPs(double v, const char *unit) {
        if (v < 1000.0) {
            return fmt::format("{:.1f} {}/s", v, unit);
        } else if (v < 1000000.0) {
            return fmt::format("{:.1f} K{}/s", v / 1000.0, unit); 
        } else if (v < 1000000000.0) {
            return fmt::format("{:.1f} M{}/s", v / 1000000.0, unit);
        } else {
            return fmt::format("{:.1f} G{}/s", v / 1000000000.0, unit);
        }
    }

// Name: formatSize()
// Desc: Format a double in K,M,G,.. per sec
    std::string formatSize(size_t size) {
        if (size < 1024UL) {
            return fmt::format("{} Bytes", size);
        } else if (size < 1048576UL) {
            return fmt::format("{:.1f} KB", (double)size / 1024.0);
        } else if (size < 1073741824UL) {
            return fmt::format("{:.1f} MB", (double)size / 1048576.0);
        } else {
            return fmt::format("{:.1f} GB", (double)size / 1073741824.0);
        }
    }

// Name: formatTime()
// Desc: Format time in HH:MM:SS
    std::string formatTime(float t) {
        return formatTime(static_cast<double>(t));
    }

    std::string formatTime(double t) {
        int nbSec = (int)(t + 0.5f);
        return fmt::format("{:02d}:{:02d}:{:02d}",
            nbSec / 3600,
            (nbSec % 3600) / 60,
            nbSec % 60);
    }
    
    std::string GetCurrentDateTime() {
    // Get the current time point
    auto now = std::chrono::system_clock::now();

    // Convert to time_t (a system time type)
    std::time_t currentTime = std::chrono::system_clock::to_time_t(now);

    // Convert to a broken-down local time (i.e., local time representation)
    std::tm localTime = *std::localtime(&currentTime);

    // Create a string stream to format the date and time
    std::ostringstream oss;
    oss << std::put_time(&localTime, "%Y-%m-%d %H:%M:%S");

    return oss.str();
    }
}

