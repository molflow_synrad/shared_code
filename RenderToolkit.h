#pragma once

#include <optional>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <string>
#include <optional>
#include <tuple>
#include "GLAppGui/GLMatrix.h"
#include "GLAppGui/GLToolkit.h"

class Vector3d;

namespace RenderToolkit {
	void SetMaterial(GLMATERIAL* mat);
  	std::optional<ScreenCoord> Get2DScreenCoord_currentMatrix(const Vector3d& p);
	std::optional<ScreenCoord> Get2DScreenCoord_fast(const Vector3d& p, const GLMatrix& mvp, const GLVIEWPORT& viewPort);
	void LookAt(const Vector3d& Eye, const Vector3d& camPos, const Vector3d& Up, const double handedness);
	void PerspectiveLH(double fovy, double aspect, double zNear, double zFar);
  	float GetCamDistance(GLfloat *mView,double x,double y,double z); //unused
  	float GetVisibility(double x,double y,double z,double nx,double ny,double nz); //unused
	void DrawCoordinateAxes(double vectorLength, double headSize = 0.0, bool colored = false);
	void DrawVector(double x1, double y1, double z1, double x2, double y2, double z2, const double headSize = 0.0); //Auto-choose a normal and draw vector
	void DrawVector(const Vector3d& start, const Vector3d& end, const Vector3d& normal, double headSize = 0.0); //Draw vector with passed normal
};
