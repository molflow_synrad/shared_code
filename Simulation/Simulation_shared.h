#pragma once
#include <optional>
#include <mutex>

class GlobalSimuState;

[[nodiscard]] std::optional<std::unique_lock<std::timed_mutex>> GetHitLock(GlobalSimuState* simStatePtr, size_t waitMs);
