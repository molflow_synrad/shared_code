//Common Molflow-Synrad implementations. Definitions still in each project's src/Simulation/Particle.h

#include "../../src/Simulation/Particle.h" //Molfow or Synrad
#include "Particle_shared.h"
#include "Simulation_shared.h"
#include "Helper/MathTools.h"
#include "GLAppCore/GLMathTools.h"
#include <cstddef> //size_t

#ifdef MOLFLOW
using namespace MFSim;
#include "../../src/Simulation/MolflowSimGeom.h"
#endif

#ifdef SYNRAD
using namespace SRSim;
#include "../../src/Simulation/SynradSimGeom.h"
#endif

bool ParticleTracer::UpdateMCHits(const std::shared_ptr<GlobalSimuState> globalState,
    std::string& myStatus, std::mutex& statusMutex, size_t timeout_ms) {

    statusMutex.lock();
    myStatus = "Waiting to access global hit counter...";
    statusMutex.unlock();

    auto lock = GetHitLock(globalState.get(), timeout_ms);
    if (!lock) return false;

    statusMutex.lock();
    myStatus = "Adding local hits to global hit counter...";
    statusMutex.unlock();

    //Global hits
    globalState->globalStats.globalHits += tmpState->globalStats.globalHits;
#ifdef MOLFLOW
    globalState->globalStats.distTraveled_total += tmpState->globalStats.distTraveled_total;
    globalState->globalStats.distTraveledTotal_fullHitsOnly += tmpState->globalStats.distTraveledTotal_fullHitsOnly;
#endif
    totalDesorbed += tmpState->globalStats.globalHits.nbDesorbed;

    //Leaks
    for (size_t leakIndex = 0; leakIndex < tmpState->globalStats.leakCacheSize; leakIndex++)
        globalState->globalStats.leakCache[(leakIndex + globalState->globalStats.lastLeakIndex) %
        LEAKCACHESIZE] = tmpState->globalStats.leakCache[leakIndex];
    globalState->globalStats.nbLeakTotal += tmpState->globalStats.nbLeakTotal;
    globalState->globalStats.lastLeakIndex =
        (globalState->globalStats.lastLeakIndex + tmpState->globalStats.leakCacheSize) % LEAKCACHESIZE;
    globalState->globalStats.leakCacheSize = std::min(LEAKCACHESIZE, globalState->globalStats.leakCacheSize +
        tmpState->globalStats.leakCacheSize);

    // HHit (Only prIdx 0)
    if (particleTracerId == 0) {
        for (size_t hitIndex = 0; hitIndex < tmpState->globalStats.hitCacheSize; hitIndex++)
            globalState->globalStats.hitCache[(hitIndex + globalState->globalStats.lastHitIndex) %
            HITCACHESIZE] = tmpState->globalStats.hitCache[hitIndex];

        if (tmpState->globalStats.hitCacheSize > 0) {
            globalState->globalStats.lastHitIndex =
                (globalState->globalStats.lastHitIndex + tmpState->globalStats.hitCacheSize) % HITCACHESIZE;
            globalState->globalStats.hitCache[globalState->globalStats.lastHitIndex].type = HIT_LAST; //Penup (border between blocks of consecutive hits in the hit cache)
            globalState->globalStats.hitCacheSize = std::min(HITCACHESIZE, globalState->globalStats.hitCacheSize +
                tmpState->globalStats.hitCacheSize);
        }
    }

    //Global histograms
    globalState->globalHistograms += tmpState->globalHistograms;

    // Facets
    globalState->facetStates += tmpState->facetStates;

#ifdef SYNRAD
    if (globalState->globalStats.globalHits.nbDesorbed > 0 && model->sp.nbTrajPoints) {
        model->no_scans = (double)globalState->globalStats.globalHits.nbDesorbed / (double)model->sp.nbTrajPoints;
    }
    else {
        model->no_scans = 1.0;
    }
#endif

    return true;
}

bool ParticleTracer::UpdateHitsAndLog(const std::shared_ptr<GlobalSimuState> globalState, const std::shared_ptr<ParticleLog> particleLog,
    ThreadState& myState, std::string& myStatus, std::mutex& statusMutex, size_t timeout_ms) {

    bool lastHitUpdateOK = UpdateMCHits(globalState,myStatus, statusMutex, timeout_ms);

    // only 1, so no reduce necessary
    if (particleLog) UpdateLog(particleLog, myStatus, statusMutex, timeout_ms);

    // At last delete tmpCache

    if (lastHitUpdateOK) {
        statusMutex.lock();
        myStatus = "Resetting temporary local results...";
        statusMutex.unlock();
        tmpState->Reset();
    }
    return lastHitUpdateOK;
}

bool ParticleTracer::UpdateLog(const std::shared_ptr<ParticleLog> globalLog,
    std::string& myStatus, std::mutex& statusMutex, size_t timeout) {
    if (!tmpParticleLog->pLog.empty()) {
        statusMutex.lock();
        myStatus = "Waiting to access global particle log...";
        statusMutex.unlock();
        if (!globalLog->particleLogMutex.try_lock_for(std::chrono::milliseconds(timeout))) {
            return false;
        }
        statusMutex.lock();
        myStatus = "Adding local particle log to global...";
        statusMutex.unlock();
        size_t writeNb = model->otfParams.logLimit - globalLog->pLog.size();
        GLApp_MathTools::Saturate(writeNb, 0, tmpParticleLog->pLog.size());
        globalLog->pLog.insert(globalLog->pLog.end(), tmpParticleLog->pLog.begin(), tmpParticleLog->pLog.begin() + writeNb);
        globalLog->particleLogMutex.unlock();
        tmpParticleLog->clear();
        tmpParticleLog->pLog.shrink_to_fit();
        tmpParticleLog->pLog.reserve(std::max(model->otfParams.logLimit - globalLog->pLog.size(), (size_t)0u));
    }

    return true;
}

void ParticleTracer::LogHit(SimulationFacet* f) {
    //if(omp_get_thread_num() != 0) return; // only let 1 thread update
    if (model->otfParams.enableLogging &&
        model->otfParams.logFacetId == f->globalId &&
        tmpParticleLog->pLog.size() < tmpParticleLog->pLog.capacity()) {
        ParticleLoggerItem log;
        log.facetHitPosition = Vector2d(facetHitDetails[f->globalId].colU, facetHitDetails[f->globalId].colV);
        std::tie(log.hitTheta, log.hitPhi) = CartesianToPolar(ray.direction, f->sh.nU, f->sh.nV, f->sh.N);
        log.oriRatio = oriRatio;
        log.particleDecayMoment = expectedDecayMoment;
#ifdef MOLFLOW
        log.time = ray.time;
        log.velocity = velocity;
#endif
#ifdef SYNRAD
        log.energy = energy;
        log.dF = dF;
        log.dP = dP;
#endif
        tmpParticleLog->pLog.push_back(log);
    }
}

void ParticleTracer::RecordLeakPos() {
    // Source region check performed when calling this routine
    // Record leak for debugging
    RecordHit(HIT_REF);
    RecordHit(HIT_LAST);
    if (tmpState->globalStats.leakCacheSize < LEAKCACHESIZE) {
        tmpState->globalStats.leakCache[tmpState->globalStats.leakCacheSize].pos = ray.origin;
        tmpState->globalStats.leakCache[tmpState->globalStats.leakCacheSize].dir = ray.direction;
        ++tmpState->globalStats.leakCacheSize;
    }
}