#include "Simulation_shared.h"

#ifdef MOLFLOW
#include "Simulation/MolflowSimGeom.h"
#endif

#ifdef SYNRAD
#include "Simulation/SynradSimGeom.h"
#endif

//Constructs a lock guard for timed mutex
//Returns false if lock not successful
//Otherwise returns the unique_lock that auto-unlocks the mutex when destroyed
[[nodiscard]] std::optional<std::unique_lock<std::timed_mutex>> GetHitLock(GlobalSimuState* simStatePtr, size_t waitMs) //not shared_ptr as called from within class as well
{
    std::unique_lock<std::timed_mutex> lock(simStatePtr->simuStateMutex, std::chrono::milliseconds(waitMs));
    if (!lock.owns_lock()) {
        return std::nullopt; //couldn't acquire lock, timed out
    }
    else {
        return lock;
    }
}