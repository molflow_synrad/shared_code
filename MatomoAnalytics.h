#pragma once

#define PUBLIC_WEBSITE "https://molflow.docs.cern.ch/"
#define DOWNLOAD_PAGE "https://molflow.docs.cern.ch/downloads/"
#define MATOMO_SITE_ID "297"
#define MATOMO_REQUEST_TARGET "https://webanalytics.web.cern.ch/matomo.php"