#include "RenderToolkit.h"
#include "Vector.h"
#include "Helper/MathTools.h"

namespace RenderToolkit {
	void SetMaterial(GLMATERIAL* mat) {
		float acolor[] = { mat->Ambient.r, mat->Ambient.g, mat->Ambient.b, mat->Ambient.a };
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, acolor);
		float dcolor[] = { mat->Diffuse.r, mat->Diffuse.g, mat->Diffuse.b, mat->Diffuse.a };
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, dcolor);
		float scolor[] = { mat->Specular.r, mat->Specular.g, mat->Specular.b, mat->Specular.a };
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, scolor);
		float ecolor[] = { mat->Emissive.r, mat->Emissive.g, mat->Emissive.b, mat->Emissive.a };
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, ecolor);
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mat->Shininess);
		glColor4f(mat->Ambient.r, mat->Ambient.g, mat->Ambient.b, mat->Ambient.a);
	}

    std::optional<ScreenCoord> Get2DScreenCoord_currentMatrix(const Vector3d &p) {
		GLfloat mProj[16];
		GLfloat mView[16];
		GLVIEWPORT viewPort;
		
		// Compute location on screen
		glGetFloatv(GL_PROJECTION_MATRIX , mProj);
		glGetFloatv(GL_MODELVIEW_MATRIX , mView);
		glGetIntegerv(GL_VIEWPORT,(GLint *)&viewPort);

		GLMatrix proj; proj.LoadGL(mProj);
		GLMatrix view; view.LoadGL(mView);
		GLMatrix mvp; mvp.Multiply(&proj,&view);

  		return Get2DScreenCoord_fast(p, mvp, viewPort);
    }

    std::optional<ScreenCoord> Get2DScreenCoord_fast(const Vector3d& p, const GLMatrix& mvp, const GLVIEWPORT& viewPort) {
		float rx, ry, rz, rw;
		mvp.TransformVec((float)p.x, (float)p.y, (float)p.z, 1.0f, &rx, &ry, &rz, &rw);
		if (rw <= 0.0f) return std::nullopt; //behind camera

		return ScreenCoord((int)(((rx / rw) + 1.0f) * (float)viewPort.width / 2.0f),
			(int)(((-ry / rw) + 1.0f) * (float)viewPort.height / 2.0f));

	}


	void LookAt(const Vector3d& Eye, const Vector3d& camPos, const Vector3d& Up, const double handedness) {
		//handedness =  -1: left handed
		// Create and load a left- or right-handed view matrix

		Vector3d Z = (camPos - Eye).Normalized();
		Vector3d X = -handedness * CrossProduct(Up, Z).Normalized();
		Vector3d Y = -handedness * CrossProduct(Z, X);

		double dotXE = Dot(Eye, X);
		double dotYE = Dot(Eye, Y);
		double dotZE = Dot(Eye, Z);

		float glViewMatrix[] =
		{ (float)X.x,   (float)Y.x,   (float)Z.x,   0.0f,
		(float)X.y,   (float)Y.y,   (float)Z.y,   0.0f,
		(float)X.z,   (float)Y.z,   (float)Z.z,   0.0f,
		(float)-dotXE, (float)-dotYE, (float)-dotZE, 1.0f };

		glLoadMatrixf(glViewMatrix);

	}

	void PerspectiveLH(double fovy, double aspect, double zNear, double zFar) {
		// Create and load a left handed proj matrix
		double fov = (fovy / 360.0) * (2.0 * PI);
		double f = cos(fov / 2.0) / sin(fov / 2.0);

		GLfloat mProj[16];

		mProj[0] = (float)(f / aspect); mProj[1] = 0.0f; mProj[2] = 0.0f; mProj[3] = 0.0f;
		mProj[4] = 0.0f; mProj[5] = (float)(f); mProj[6] = 0.0f; mProj[7] = 0.0f;
		mProj[8] = 0.0f; mProj[9] = 0.0f; mProj[10] = (float)((zFar + zNear) / (zFar - zNear)); mProj[11] = 1.0f;
		mProj[12] = 0.0f; mProj[13] = 0.0f; mProj[14] = (float)((2.0 * zNear * zFar) / (zNear - zFar)); mProj[15] = 0.0f;

		glLoadMatrixf(mProj);
	}

    float GetCamDistance(GLfloat *mView, double x, double y, double z) { //unused
		float rx,ry,rz,rw;
		GLMatrix view; view.LoadGL(mView);
		view.TransformVec((float)x,(float)y,(float)z,1.0f,&rx,&ry,&rz,&rw);
		return sqrtf(rx*rx + ry*ry + rz*rz);
    }

    float GetVisibility(double x, double y, double z, double nx, double ny, double nz) { //unused
		GLfloat mView[16];
		float rx,ry,rz,rw;
		float ntx,nty,ntz,ntw;

		glGetFloatv( GL_MODELVIEW_MATRIX , mView );
		GLMatrix view; view.LoadGL(mView);
		view.TransformVec((float)x,(float)y,(float)z,1.0f,&rx,&ry,&rz,&rw);
		view.TransformVec((float)nx,(float)ny,(float)nz,0.0f,&ntx,&nty,&ntz,&ntw);
		return rx*ntx + ry*nty + rz*ntz;
    }

    void DrawCoordinateAxes(double vectorLength, double headSize, bool colored)
    {
        Vector3d O(0.0, 0.0, 0.0);
		Vector3d X(1.0, 0.0, 0.0);
		Vector3d Y(0.0, 1.0, 0.0);
		Vector3d Z(0.0, 0.0, 1.0);
		Vector3d X_end = vectorLength * X;
		Vector3d Y_end = vectorLength * Y;
		Vector3d Z_end = vectorLength * Z;
		if (colored) glColor3f(1.0f, 0.0f, 0.0f);
		DrawVector(O, X_end, Y, headSize);
		if (colored) glColor3f(0.0f, 1.0f, 0.0f);
		DrawVector(O, Y_end, Z, headSize);
		if (colored) glColor3f(0.6f, 0.6f, 1.0f);
		DrawVector(O, Z_end, X, headSize);
		glPointSize(4.0f);
		glBegin(GL_POINTS);
		glColor3f(0.4f, 0.8f, 0.8f);
		glVertex3d(0.0, 0.0, 0.0);
		glEnd();
    }

    void DrawVector(double x1, double y1, double z1, double x2, double y2, double z2, const double headSize) {
		Vector3d start(x1, y1, z1);
		Vector3d end(x2, y2, z2);
		Vector3d diff = end - start;
		Vector3d diffNorm = diff.Normalized();
		Vector3d normal;

		// Choose a normal vector
		if (std::abs(diff.x) > 1e-3) {
			// Oy
			normal = Vector3d(diffNorm.z, diffNorm.y, -diffNorm.x);
		}
		else if (std::abs(diff.y) > 1e-3) {
			// Oz
			normal = Vector3d(diffNorm.y, -diffNorm.x, diffNorm.z);
		}
		else {
			// Ox
			normal = Vector3d(diffNorm.x, diffNorm.z, -diffNorm.y);
		}
		DrawVector(start, end, normal, headSize);
	}


	void DrawVector(const Vector3d& start, const Vector3d& end, const Vector3d& parallel, double headSize) {
		Vector3d diff = end - start;
		if (headSize < 1E-10) headSize = .1 * diff.Norme(); //Default head size 10% of vector length
		Vector3d headDiff = headSize * diff.Normalized();
		Vector3d arrowEnd = end - headDiff;
		Vector3d reducedParallel = headSize * parallel;
		Vector3d p2 = end + reducedParallel - headDiff;
		Vector3d p3 = end - reducedParallel - headDiff;

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_LIGHTING);
		glDisable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		glBegin(GL_LINES);

		glVertex3d(start.x, start.y, start.z);
		glVertex3d(arrowEnd.x, arrowEnd.y, arrowEnd.z);

		glVertex3d(end.x, end.y, end.z);
		glVertex3d(p2.x, p2.y, p2.z);

		glVertex3d(end.x, end.y, end.z);
		glVertex3d(p3.x, p3.y, p3.z);

		glVertex3d(p2.x, p2.y, p2.z);
		glVertex3d(p3.x, p3.y, p3.z);

		glEnd();
	}

}